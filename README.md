# handbook.volkis.com.au

The handbook portal for Volkis hosted at: https://handbook.volkis.com.au/

Generated using Jekyll static site generator.

## Build

```bash
bundle install
bundle exec jekyll build  # development build ... or ...
JEKYLL_ENV=production bundle exec jekyll build  # production build
```

## Development server

```bash
bundle exec jekyll serve --H 0.0.0.0 --livereload
```