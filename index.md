---
title: Handbook
layout: page
---

## Introduction

Welcome to the Volkis Handbook! Here, we store all the information for our own employees, customers, industry peers and anyone who's interested. We have a philosophy of being **open but secure** so, as long as the data is not confidential, we try to publish it.

We're constantly adding content and fixing issues, so please contribute if you'd like! Merge requests are more than welcome 🙏: <https://gitlab.com/volkis/handbook.volkis.com.au/>.

Enjoy perusing the page. 🙂

{% for section in site.data.toc %}
{% assign name = section[0] %}
{% assign item = section[1] %}

## {{name}}

{{item.blurb}}

{% include toc.html items = item.items %}

{% endfor %}
