---
title: Service catalogue
---

This page covers the different services offered by Volkis, a description, the benefits, and additional details on the service.

## Offensive Security

### Penetration testing

For your business' security, it is important to identify vulnerabilities that could allow adversaries access to your systems and data. Vulnerabilities that put your business at significant risk of a compromise are not only damaging for you. They could have an impact on your employees and your customers. Do you know your risk profile and what types of adversaries might attack you?

Penetration testing is our bread and butter! Volkis "pentesting" services will find the vulnerabilities that could be used to hack your networks, web applications, mobile applications, wireless networks, core services and cloud environments. A skilled penetration tester will use the same tools, techniques and instincts that that an adversary would to break into your systems.

What vulnerabilities exist in my systems? Do I need to worry about it? What could attackers do to me if exploited? What can I do to prevent this? All these questions can be answered with a penetration test.

#### Web application penetration testing

Make sure your bespoke web applications are resilient against attackers.

[Web application penetration testing methodology](/services/methodologies/web-application-penetration-test/)

#### Internal penetration testing

Find vulnerabilities from the perspective of an attacker inside your network.

[Internal penetration testing methodology](/services/methodologies/internal-penetration-test/)

#### External penetration testing

Test your internet-facing servers and services against external threats.

[External penetration testing methodology](/services/methodologies/external-penetration-test/)

#### Mobile application penetration testing

Find those hidden vulnerabilities lurking inside your bespoke mobile apps.

#### Wireless penetration testing

See if hackers can break into your network through the wireless infrastructure.

[Wireless penetration testing methodology](/services/methodologies/wireless-penetration-test/)

#### Microsoft 365 penetration testing

Curious to see what your tenant looks like from the perspective of a hacker? We apply our knowledge of Azure and M365 configurations to try and identify vulnerable paths that allow us to escalate access and compromise your tenant.

#### Continuous penetration testing

Work with us to establish regular penetration testing of your applications or infrastructure. Year-round coverage of your attack surface ensures penetration testing is not just a "point-in-time" exercise.

### Modular services

Volkis has a modular ecosystem of auxiliary services. These services can be combined with a penetration test to better meet your goals or to increase visibility. Just like the Power Rangers combined into a more powerful robot, our services combine to give a better, more accurate result!

#### Executive and board debrief

Cyber security is becoming a bigger issue for corporate executives and boards, with boards demanding that the risks associated with cyber security are managed as they are being held accountable for those risks.

In Volkis' executive and board debrief, we use a principal consultant to analyse the results, contextualise the results in the organisation and industry, and provide the information in a presentation suitable for an executive or board audience. With exposure to both business and technical security, our principal consultant can bridge the gap to give your executive and board the information they need to make effective decisions to manage cyber security risk.

#### Code assisted testing

This is a _free_ addon for all of our application centric penetration testing services. By giving Volkis access to the source code our consultants can gain a broader understanding of the application, and find issues that would potentially be missed in a black-box test. See information about why we believe this can give you some of the best value for your penetration test [here](https://www.volkis.com.au/blog/how-to-get-the-most-out-of-your-pentest/#whitebox-testing).

#### Developer/administrator workshop

The results of a penetration test are a learning opportunity for all of the people involved in setting up and administering the systems that are tested. They can use the results to find out what issues there were, learn from those issues, and avoid those issues in future tests.

In our developer/administrator workshop, we sit with those technical personnel, explain each vulnerability, give background causes of the vulnerability, and teach actionable ways of improving development and administration to ensure that the vulnerabilities don't reoccur in the future.

#### Custom reporting

Want us to contextualise risk into your own risk matrix? Prefer a CSV or Markdown format? We’re happy modify our report into something that will work best for you, or even report directly into your ticketing system.

#### Additional retesting

Volkis provides a complementary retest for internet-accessible vulnerabilities. For further validation and on-site retesting, Volkis can provide additional retesting to ensure that the fixes you have put in for the vulnerabilities we find work. The consultant will re-run the exploits and test cases for the identified vulnerabilities to see whether or not the vulnerabilities still work.

### Detection alert testing

As offensive security testers we have to be aware of what alerts we set off and when. Usually our goal is to set off fewer alarms, but that is not always ideal when you would like to know more about whether those alerts are properly set and where those alerts are sent when they are triggered.

In our detection alert testing service, we will deliberately set off alerts in a methodical way so you can see those alerts in action. This could include planting malware, sending a "command and control" request to the internet, or performing malicious activity on a user account. These activities should be picked up by your security systems, allowing you to gain familiarity with the alerts and ensure they are working properly.

### Red team engagement

Volkis red team services will target your organisation over an extended period using skilled attackers, infrastructure specifically tailored to the engagement, and social engineering attacks to compromise sensitive information and core business services.

The engagement will be performed over a window period, usually 2-3 months. Our attackers will use drip scanning, passive investigation, and evasion techniques to get by your detection and response systems, breaking into your environment without being seen.

The targets will be information and systems that are relevant to your organisation and likely targets for attackers. Instead of targets such as “compromise an administration account”, we look for key business services such as your CRM, financial systems, critical applications, and your web infrastructure to identify real, meaningful impact to your organisation. This allows you to put the results into context and provides meaningful feedback to your executive and board.

### Social engineering

With Volkis social engineering exercises you can test the security awareness of your users, and incorporate the results into an ongoing security awareness programme.

Our social engineering capability includes:

* **Phishing attacks**: We can send malicious emails to your users that will attempt to trick them into downloading software, providing credentials, or visiting a particular web site. Phishing attacks can be sent to a large number of users in a cost effective manner.
* **Vishing attacks (phone calls)**: We can test your employees or service desk to identify if they are likely to give out privileged information or credentials over the phone, or if they would be willing to undertake potentially damaging actions from an unknown source.
* **SMishing attacks (SMS)**: We can test your users’ abilities to detect a malicious text by sending SMS messages to their company phones. This text may instruct them to either download a malicious app or provide credentials.
* **Malicious USBs**: We can provide malicious USBs that, when inserted into a user’s laptop or PC and executed, will call back to Volkis. This will test the likelihood of your users being tricked by a malicious attacker with physical access to your premises, or by someone who could send a USB stick through post.

### Physical intrusion

Sometimes the easiest way to get access to your sensitive data is to simply walk in and take it. Volkis’ physical intrusion services will test your organisation to see if that is possible.

The services will first test the resilience of your access control and physical protection systems to see if they can be bypassed or broken. Can your passes be duplicated by someone standing next to them in the elevator? Can someone simply walk through a back door or open a window?

The consultant will then attempt to use social engineering techniques such as tailgating to gain physical access to the premises.

If access is gained, the consultant will collect evidence to see what can be gained from that physical access. Depending on the rules for the engagement, the consultant may take pictures of desks, access sensitive areas, or connect to the network using a laptop or a concealed network tap.

At the end of the engagement, the consultant will outline any weaknesses and provide actionable recommendations for improving the security of the physical premises. This could include improving access control, improving procedures and processes for handling entrants, or user education.

## Secure Config Reviews

### On-premises technologies

* Microsoft Active Directory
* Windows 10/11 SOE

### Cloud technologies

In a modern IT environment, the perimeter is becoming less defined, with organisations employing Software-as-a-Service and Infrastructure-as-a-Service. Your cloud environments could lead to your sensitive data being compromised and your key business services being exposed to potential availability impact.

In particular, AWS and Azure environments have complex permissions structures that could be manipulated by attackers to gain unauthorised access. Working out these permissions and the effects that they could cause requires in-depth knowledge of the security implications of choices made in these environments.

Volkis secure config reviews will review your cloud environment for common misconfigurations, permissions issues, insecure service usage, and insecure design. Our assessment services can cover:

* Amazon AWS
* Microsoft Azure Cloud
* Microsoft 365
* Microsoft Exchange Online

## Security strategy & Compliance

Volkis security strategy advisory services will help you align the security of your organisation to your organisation’s strategic objectives. Whether it is tightening up the security to align to your risk appetite, or using security as an enabler for new business initiatives, our strategy advice will allow you to achieve your objectives and provide a greater level of security for lower cost.

Using security professionals with decades of experience, Volkis will provide high level analysis and advice, specific to your industry, organisation, and unique circumstances. We will help you define a 3-5 year strategy allowing long term stability with consistent expectation of security, fulfilling legal and compliance requirements and maintaining your customers’ trust.

### ISO 27001

Show your customers you have your security under control with the international standard ISO27001. Recognised across the world, the standard provides a risk based approach that will optimise your security controls, leaving you with efficient and effective security. In addition, the standard is auditable, meaning you can show your customers your ISO27001 accreditation.

Volkis provides gap analysis and implementation services for ISO27001. We will walk you through the complete implementation of the framework in your environment, dealing with the different stakeholders in the business to ensure the project is a success.

### NIST Cybersecurity Framework

Volkis can help your organisation gain alignment and maintain compliance with the NIST Cybersecurity Framework. This framework provides a sensible baseline of security that is appropriate for all organisations. It is split into five sections: Identify, Protect, Detect, Respond and Recover.

As part of our NIST compliance services, Volkis can guide you through using the NIST framework to enhance and optimise the security of your organisation. Whether you aim for full compliance, or you incorporate a subset of the framework into your security strategy, we can provide the visibility, analysis, policy and process development, and advisory services you require.

### ACSC Essential 8

The ACSC has published eight essential security controls that it believes are appropriate for all organisations. These are:

* Application whitelisting
* Patch applications
* Configure Microsoft Office macro settings
* User application hardening
* Restrict administrative privileges
* Patch operating systems
* Multi-factor authentication
* Daily backups

Volkis has built an assessment methodology that can test your organisation against the ACSC Essential Eight Maturity Model. This assessment methodology uses automated and manual tools and techniques to test which controls are in place and which controls and what maturity level each control aligns to in your organisation.

Volkis consultants can then provide recommendations and advisory services for improving the maturity level of your organisation and gain alignment with the standard. This can be used to show your clients and government agencies you have the baseline of security in place.

<!-- ## Education

### Executive education

Executives and boards are now accountable for cyber security risks. Volkis executive education provides the information that they need to judge and account for those risks.

As part of our executive education programme, Volkis provides in-person relevant information and analysis. This can be performed in one or multiple presentations for your executive.

The presentations will be tailored to your organisation, but will usually cover:

* An overview of the security landscape;
* An overview of security within your industry;
* Results of security initiatives such as security strategy, penetration testing, and red team engagements;
* The legal and regulatory requirements for your business;
* An overview of how hacking works, and some things attackers can do to compromise systems;
* Recommendations for securing your personal systems. -->
