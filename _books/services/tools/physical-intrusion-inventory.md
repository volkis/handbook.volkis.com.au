---
name: Physical Intrusion Kit Inventory
---

We keep a box of tools used for our Physical Intrusion engagements. This box is shipped around the country if needed to help consultants break into sites like offices, warehouses, factories, and plants. Think of the gadgets like James Bond's if he bought them from Aldi.

## What's in the box?

_Queue Brad Pitt from Se7en._

| Item                               | Purpose                                                     |
| ---------------------------------- | ----------------------------------------------------------- |
| Brockhage Pick Gun x1              | Quick way to try and pick locks                             |
| Ruler (cut) x1                     | Used as a shim that can be slid over and behind door plates |
| Door Shim (lg) x1                  | Used to shim door latches                                   |
| Door Shim (sm) x1                  | Used to shim door latches                                   |
| J-tool x1                          | Used to bypass door locks with accessible thumb latches     |
| Commercial Door Hook x1            | Used to bypass vulnerable commercial door locks             |
| Elevator keys (set of 10)          | Some elevators can be controlled with known key cuts        |
| Mirror x1                          | Slide under the door to see what's on the other side        |
| Ratcheting screwdriver x1          | Multi-purpose                                               |
| Pry tools (set of 3)               | Used to make a gap in doors for shimming or bypass          |
| Hook & pick (set of 4)             | Multi-purpose                                               |
| Metal wire                         | Bypass some latch guards / door plates                      |
| Clipboard x1                       | Used for appearance when impersonating technicians/auditors |
| Lanyards: Staff, visitor, unmarked | Used for appearance to look legitimate                      |
| Hi-vis vest x2                     | Used for appearance to look legitimate                      |
| Reusable cable ties                | Securely hold the box closed                                |

## Extras

These aren't in the box but can also be used for physical intrusion when needed.

- Proxmark3
- Long-range RFID reader
- Blank RFID cards
- Under-the-door tool
