---
title: 'Mobile pentest: Getting started'
---

Mobile testing is a pain in the proverbials. Hopefully this helps you in getting started.

## Physical vs virtual devices

As a general rule, it is advisable to test on a physical device. Although Android emulators are generally good, they aren't a 100% representation of a physical device (think fingerprint/face scanners). This guide does not cover setting up a virtual device (yet).

## Jailbreaking/Rooting

The first thing we need to do is jailbreak/root the test device, if it hasn't been already. This will allow you to test the application more effectively.

### iOS

#### Install checkra1n

**checkra1n** is one of the most consistent jailbreak exploits, but do be aware that it does not persist across restarts. Every single time root execution is required the device will need to be booted and re-exploited with checkra1n.

Currently checkra1n only supports running on Linux and macOS. There is a push towards a Windows GUI but progress is slow. Until then, if your host is not Linux then you should use your favourite flavour of Linux live-boot and download checkra1n from the [official website](https://checkra.in/).

Once your IOS device is connected and checkra1n is running, the process is very simple. Follow the prompts in checkra1n (it'll tell you everything you need to know) and you should have a rooted iPhone up and running very quickly.

Once our iPhone is rooted and turned on you can access a root console by directly SSHing into the phone with `root`/`alpine` credentials.

#### Install Frida server

To install Frida we need to add the [https://build.frida.re](https://build.frida.re) repository to Cydia. To do this simply open Cydia, click on the Sources tab, click edit in the top right and then add in the top left. Once this is added install the Frida package. Frida should now start automatically.

### Android

On your testing machine, install `adb` and `fastboot` from android's platform SDK tools if they aren't already installed:

Ubuntu:

```
$ apt install android-tools-adb android-tools-fastboot
```

Arch:

```
$ pacman -S android-tools
```

Windows:

Download the latest release of the SDK Platform-tools [here](https://developer.android.com/studio/releases/platform-tools).

#### Enable developer options

To access the phone over USB to begin the rooting process, you will need to enable developer options and enable USB debugging.

1. In **Settings > About phone**, scroll down to **Build number** (alternatively it might be **Kernel Version**) and tap it seven times until a notification comes up saying developer options are unlocked.
2. In **Settings > Developer options**, enable **USB debugging**. Click **OK** on the prompt and **OK** again if another prompt with an RSA fingerprint comes up.

You can test to see if it's working correctly by connecting the testing device to USB and running:

```
$ adb devices
```

A serial number should show up, in which case USB debugging works correctly. You may need to accept the testing machine's RSA fingerprint if the command fails.

#### Unlock bootloader

In **Settings > Developer options**, enable **OEM unlocking**.

You may need to refer to your testing device's manufacturer for more details on the unlocking process, as some makes have different requirements (such as requiring an unlock code).

Generally, the process is as follows:

1.  Boot into fastboot. You can do this manually by holding the **Power** and **Volume Down** buttons for ten seconds. Otherwise, you can simply run:

        $ adb reboot fastboot

2.  Unlock your bootloader with one of these variations:

        $ fastboot oem unlock

        $ fastboot flashing unlock

        # For Motorolas. Will need to input unlock codes in Motorola website to get unique key
        $ fastboot oem get_unlock_data
        $ fastboot oem unlock <UNIQUE_KEY>

        # For HTC. Will need to input unlock code in HTC website to get unique unlock token
        $ fastboot oem get_identifier_token
        $ fastboot oem unlocktoken Unlock_code.bin

    You should receive an **OKAY** message in your terminal if successful.

3.  Reboot the phone. Do it manually from the fastboot menu or run:

        $ fastboot reboot

#### Install TWRP

TWRP (Team Win Recovery Project) is a custom recovery used for rooting your device and replacing the firmware with a custom ROM.

1.  [Search for your device here](https://twrp.me/Devices/) and download the latest version of TWRP. Reboot your phone into the bootloader with:

        $ adb reboot boatloader

2.  Flash the TWRP image onto the device:

        $ fastboot flash recovery <NAME_OF_TWRP_FILE>.img

3.  Reboot the device:

        $ fastboot reboot

4.  Confirm TWRP flashed correctly:

        $ adb reboot recovery

If all went well, you should now be in the TWRP menu.

#### Install Magisk

Magisk is a beautiful little set of tools, including providing root access and bypassing root detections and system integrity checks.

1.  Download the latest ZIP file from the [GitHub repo](https://github.com/topjohnwu/Magisk) to your testing machine.
2.  Within TWRP, access **Advanced > ADB Sideload** and swipe to start the sideload process. Run:

        $ adb sideload <NAME_OF_MAGISK_FILE>.zip

3.  Reboot your phone. If all went well, you should now have a Magisk app installed.

4.  Finally, you can check to see if root is available:

        $ adb shell
        $ su
        # ls -l /data/data/

If these commands worked, you are good to go.

#### Install Frida server

Installing the Frida server directly onto your phone allows for dynamic instrumentation (hooking into processes to do fun things).

1.  Discover the architecture of the device:

2.  Download the [latest Frida server](https://github.com/frida/frida/releases) for your device (generally frida-server-VERSION-android-ARCHITECTURE.xz).

3.  Decompress the file:

        $ unxz <NAME_OF_FRIDA_FILE>.xz

4.  Push the server file to your device:

        $ adb root # might be required
        $ adb push <NAME_OF_FRIDA_FILE> /data/local/tmp/
        $ adb shell "chmod 755 /data/local/tmp/<NAME_OF_FRIDA_FILE>"
        $ adb shell "/data/local/tmp/<NAME_OF_FRIDA_FILE> &"

5.  If Frida is installed on your testing machine, test that the server is running by running:

        $ frida-ps -U

If you get a list of processes, you're a winner.

## Setup

### Wireless access point

With a USB WiFi card plugged in, use [create_ap](https://github.com/mhuttner/create_ap) to create an access point, e.g.:

```
$ create_ap wlan0 eth0 MyAccessPoint MyPassPhrase
```

This will create the access point `MyAccessPoint` with the password `MyPassPhrase`. A wireless network will be created on `wlan0` and NAT'd through to `eth0`.
Simply connect the phone to the access point and see if you can access the Internet.

### Burp Suite proxy

Add a new proxy listener bound to the gateway of the access point. If the access point was set up in your virtual machine and you're using Burp Suite on your host, bind the proxy listening to the host-only/VM NAT interface.

On the testing device, set up a proxy in your network settings pointing to the Burp Suite proxy. Test that it's working by trying to visit [http://burp](http://burp).

If successful, download the CA certificate and install it. [How to install a custom CA on iPhones](https://support.apple.com/en-au/HT204477). [How to install a custom CA on Android](https://portswigger.net/support/installing-burp-suites-ca-certificate-in-an-android-device).

Once installed, see if you can proxy anything while in the device's web browser. If you can, congrats! If not, either my guide sucks or you've missed something.

Open the in-scope application (if installed) and see if it functions correctly. If it does, congratulations! You've got your first finding! There's no certificate pinning. If it doesn't, it sounds like you've gotta try harder and bypass certificate pinning.

### Frida

If Frida is not already installed, run:

```
$ pip3 install frida-tools
```

#### Test Frida server

On the testing device, SSH to it, locate where you installed Frida server, and run it in the background (if it's not already running).
You could also use `adb shell` to run it on Android.

On your testing machine, see if you're able to connect to the Frida server:

```
$ frida-ps -U
```

If successful, a list of processes should be displayed.

### Sideloading application

If you're not testing the version of the application currently available on the Apple App Store/Google Play Store,
you will need to sideload it onto the device.

#### iPhone

To install and `IPA` file (even if it's unsigned) we can use the very helpful tool made by [Karen](https://akemi.ai) `appinst`. To get this tool we first need to add the [https://cydia.akemi.ai/](https://cydia.akemi.ai/) repo to Cydia. To do this simply open Cydia, click on the Sources tab, click edit in the top right and then add in the top left. Once this is done, find the package (`appinst (App Installer)`) and install it. Now the `appinst` utility will be available to use from a command line. Additionally, to install incorrectly signed mobile applications, you will need to install the `AppSync Unified` package from the same repository.

With a copy of the application IPA file on your testing machine, run (the creds should be `root`/`alpine`):

```
$ scp app-to-install.ipa root@<iphone_ip_address>:~
```

We can then SSH in and install the application

```
$ ssh root@<iphone_ip_address>
$ appinst app-to-install.ipa
```

#### Android

With a copy of the application APK on your testing machine, run:

```
$ adb install <PATH_TO_APK>
```

Check to see if it's there and opens!

## Next steps

Now you can start playing around with tools like:

- [Objection](https://github.com/sensepost/objection);
- [drozer](https://hub.docker.com/r/fsecurelabs/drozer);
- [r2flutch](https://github.com/as0ler/r2flutch);
- and more.

We also recommend looking at the [Frida Codeshares](https://codeshare.frida.re/) in order to get some real-world examples of Frida scripts.
