---
title: Setting up a testing host in AWS
---

## Why?

When you're performing a project like a penetration test, you might want to use a cloud VPS for multiple reasons. Some of the common reasons are:

- You are required to use a whitelisted IP address
- You have the need... The need for (internet) speed!
- You need an externally available host to listen for incoming connections

## Whitelisting IP addresses

Volkis has an official IP address that clients can whitelist. It always resolves from `out.volk.is`. Here it is in our Sandbox AWS environment...

![Whitelisted IP](/assets/img/whitelist-ip.png)

(Like it says, don't delete this IP after you're done testing.)

If you want clients to whitelist an IP address for testing purposes, you should strongly prefer giving them this one rather than your home/office IP address. This is done for consistency and also for security. Your home IP might rotate and be given to someone else while the client might forget to remove it from the whitelist.

The same applies for when the client asks you to test from a predefined IP addresses.

## Creating a testing instance

The Sandbox AWS environment is there specifically for this purpose. Feel free to play with what you need in here. In this guide, we'll just focus on EC2. Let's get started...

1. [Create your EC2 instance](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EC2_GetStarted.html) - size is up to you, but limit it for what you think you'll need.
2. Assign the whitelisted IP address to this instance.<br>![Assigning IP address 1](/assets/img/assign-ip1.png)
3. Choose the instance you just created.<br>![Assigning IP address 2](/assets/img/assign-ip2.png)

4. SSH to your new instance! From here you can install whatever tools you need. ⚠️ Remember to remove this instance once you're finished with it, but leave the IP (it will auto disassociate).

## Tunnelling local tools to your testing host using WireGuard (preferred)

A lot of the time you might not want to install everything you need on the testing host. Instead, to save time, you can use WireGuard to tunnel your traffic through the whitelisted IP address. Installing and using WireGuard is super simple and we'll go over that process now. Also big thanks to [Angristan on GitHub](https://github.com/angristan) for providing an [awesome installation repository](https://github.com/angristan/wireguard-install) we will use.

1. (SSH into the box if you haven't already).
2. Run the following commands to start the set up process:

```
cd /tmp/
curl -O https://raw.githubusercontent.com/angristan/wireguard-install/master/wireguard-install.sh
chmod +x wireguard-install.sh
sudo ./wireguard-install.sh
```

3. Follow the prompts to configure it how you like. Here's an example config input. <br>![Install Wireguard 1](/assets/img/wireguard-install.png)
4. It will take some time to install everything, so sit back and relax.
5. Follow the last prompts to configure the client information. You can type anything you want for **Client Name**, just something memorable. Your hacker handle is sensible here. 😉 <br>![Install Wireguard 2](/assets/img/wireguard-install-1.png)
6. Once it's done you can copy the newly created `.conf` file to your local system (check the end of the output for its location).
7. ⚠️ Don't forget to [allow your chosen WireGuard port in the AWS security group](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-security-groups.html#adding-security-group-rule) linked to your instance.

Next we're going to use the [Wireguard client](https://www.wireguard.com/install/) to import the configuration.

1. ([Install the Wireguard client](https://www.wireguard.com/install/) on your local system if you haven't already).
2. Open the WireGuard client and click on Import Profile.<br>![Import tunnel configuration](/assets/img/wireguard-client-import-config.png)
3. Copy the `.conf` file you copied earlier.
4. Click the **Activate** button to connect to the Wireguard server. Hopefully in a few seconds, you should be connected to the VPN. Now all your traffic should come out of the whitelisted IP address.
5. To make sure it is, you can try running this command: `curl ifconfig.me/ip`<br>![Outbound IP](/assets/img/outbound-ip.png)

Once you're done testing, remember to delete the host and everything associated with it like:

- The instance
- Security groups
- Keypairs
- ⚠️ **But NOT the whitelisted IP address** - if you're using other IP addresses, deleting those is fine
