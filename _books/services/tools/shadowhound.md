---
title: Bypassing AV with Shadowhound binaries
---

## Overview

Shadowhound refers to in-house Azure Pipeline that builds common malicious C# solutions using AV evasion techniques. This can be useful during an assessment if the Windows machine/environment you are working on has AV installed and if you need a C# tool. These binaries are generated every Monday morning at 8AM or if the Azure pipeline is manually run. These binaries are avaliable from an internet accessible s3 bucket. Unfortunately, AV Evasion is a cat and mouse game so there is a chance these binaries still get flagged by AV.

## I NEED BINARIES NOW TLDR ME

1. Type `!shadowhound` in slack for the URL or navigate to the [public s3 bucket URL](https://shadowhound.s3.ap-southeast-2.amazonaws.com/)  
2. Download and view the `/binaries/correlation.txt` file to figure out which binary you need
3. Download the binary you need.
4. Pray that it doesn't get flagged by AV

## Can I trust your binaries?

Short answer: no.

Longer answer: Please. Please. Please do not run random binaries or executables that you find on the internet. Rather build a binaries/executables yourself so you understand what they do. This not only applies to binaries/executables but any script or exploit proof of concept as well that you may find on the internet. Please do it yourself or read the code before running anything on your machine. You can probably convince ChatGPT to help you if you need :)

## Where are the binaries located?

The binaries are located within a internet accessible S3 bucket. A public S3 bucket was chosen as it is internet accessible and simple. So if need to try bypass AV on an internal engagement, you can quickly make a web request via CLI / Web browser and grab the binaries you need. 

You can find the public s3 URL by typing `!shadowhound` in any slack channel / chat and slackbot will happily provide you the URL. Alternatively, navigate to the [public s3 bucket URL](https://shadowhound.s3.ap-southeast-2.amazonaws.com/) 

## What is up with the weird binary naming scheme?

A result of [InvisibilityCloak](https://github.com/h4wkst3r/InvisibilityCloak) is that it renames the tool name and all references to that tool. 

![Rubeus being being renamed to Bambu](/assets/img/shadowhound-naming.png)

However, the actual binaries names are dog names from Seattle Pet Licenses because at Volkis we LOVE dogs. The dataset has been sorted, only contains unique values and turned into [Pascal case](https://en.wiktionary.org/wiki/Pascal_case) because spaces are bad in file names.

You can find the name mappings of the binaries by downloading the `/binaries/correlation.txt` file that is inside the s3 bucket.

![correlation.txt](/assets/img/shadowhound-dog-names-mapping.png)

**Note:** These binaries are generated every Monday morning at 8AM or if the Azure pipeline is manually run. So new names will be assigned every new generation.

## Which binaries do I pick?

Inside the **binaries** folder you can see the following folders:

- InvisCloak
- Confused-Invis
- Shellcode
- csproj

### InvisibilityCloak (InvisCloak)

The binaries in this folder have been obfuscated with the [InvisibilityCloak](https://github.com/h4wkst3r/InvisibilityCloak) python script. InvisibilityCloak performs the following obfuscation:

- Change the tool name
- Change the project GUID
- Obfuscate compatible strings in source code files based on obfuscation method entered by user
- Removes one-line comments (e.g. // this is a comment)
- Remove PDB string option for compiled release .NET assembly

### ConfuserEx + InvisibilityCloak (Confused-Invis)

The binaries in this folder have been obfuscated with both [InvisibilityCloak](https://github.com/h4wkst3r/InvisibilityCloak) and [ConfuserEx](https://github.com/mkaring/ConfuserEx). ConfuserEx performs a variety of protections on the binary including:

- Anti Debug Protection
- Anti Dump Protection
- Anti IL Dasm Protection
- Anti Tamper Protection
- Constants Protection
- Control Flow Protection
- Invalid Metadata Protection
- Name Protection
- Protection Hardening
- Reference Proxy Protection
- Resources Protection
- Type Scramble Protection
- Watermark

### Shellcode (Shellcode)

These text files contain shellcode that has been generated using [Donut](https://github.com/TheWover/donut) against both Invis-Cloak and Confused-Invis binaries. 

There are Invis-Cloak and Confused-Invis folder inside the Shellcode folder. At the end of the file name Invis-Cloak should have `-ic` and Confused-Invis and `-ci` respectively. 

Shellcode is hard to run and usually requires a shellcode loader/runner that you can write yourself. Most shellcode loaders/runners are written in C but they do exist in other languages as well. For an example, of how this is done you can look at the following tutorial on [ired.team](https://www.ired.team/offensive-security/code-injection-process-injection/loading-and-executing-shellcode-from-portable-executable-resources) 

For Volkis employees, check out the following repository: [Bite](https://gitlab.com/volkis/bite)

### csproj (csproj)

These text files contain base64 encoded versions of both Invis-Cloak and Confused-Invis binaries. The csproj folder also contains a `sample.csproj` file which contains C# code that can load a base64'd C# binary into memory. Before executing this technique an AMSI bypass should be run in the powershell process. If you are in need of an AMSI bypass, go to [AMSI.fail](https://amsi.fail) and keep generating bypasses until one works.

You will have to manually add the base64'd binaries and add any arguments for the binary into the "sample.csproj" file. You can then run the file using the following syntax on any Windows 10/11 host:

```
C:\Windows\Microsoft.Net\Framework64\v4.0.30319\MSBuild.exe sample.csproj
```
The text files that contain the base64'd binaries are Invis-Cloak and Confused-Invis folder inside the csproj folder. At the end of the file name Invis-Cloak binaries should have `-ic` and Confused-Invis binaries should have `-ci` respectively.
