---
title: Remediation Test Welcome Pack
owner: Finn Foulds-Cook
last_reviewed_on: 2024-07-04
---

You've just completed a project with us, and now you are wondering what the included remediation testing (retesting) looks like. Look no further!

## What is a retest?

A retest is a chance for us to come back in and check if the fixes applied to vulnerabilities successfully prevent exploitation. While it is not another completely fresh look at your application, it can still help you ensure that appropriate patches have been applied to mitigate the identified risk.

## What a retest covers

Our included remediation testing specifically covers *External and Internet-exposed Web Application vulnerabilities only*. This is to ensure that our consultants can get-in, identify if issues have been fixed and update the report in an appropriate timeline. If you are interested in having other components of the job retested, talk to our team and we can work something out, just be aware that it might not be included in the original quote.

## When does it expire?

Our retest should be booked within 3-months of receiving the report, but if this is not possible please talk to us as soon as possible and we can sort it out.

## What do we need from you?

Ensure that all of our previous testing requirements are satisfied, for example:

  - Ensuring that white-listing of IP addresses is re-enabled.
  - Providing working credentials that have the same roles as the initial testing.

This should be performed (and checked from our-side) *prior* to the day we have booked for re-testing to ensure that the retesting can be completed.

## What do you get?

We produce a new retesting report that provides details on:

 - The current status of each vulnerability - open, or closed
 - Updated risk calculations based on the mitigations applied to vulnerabilities.
 - Where applicable, details of how the consultants bypassed security patches that were not effective at preventing exploitation.
