---
title: Phishing Campaign Welcome Pack
owner: Alexei Doudkine
last_reviewed_on: 2024-05-20
---

If you're reading this, you've probably just signed up (or are interested in) a phishing campaign from us! We're about to start creating an email template and a landing page, custom made to try and trick your staff into giving us their credentials. But before then, there's some preparation to do.

## Allowlisting our phishing campaign

### Why allowlist?

In a real phishing attack, of course you wouldn't allowlist the attacker's campaign! So why do it for ours? Remember, the goal of performing a simulated phishing campaign is to test the security awareness of your employees; not to test the effectiveness of your email gateway.

### Allowlist with Microsoft 365

Microsoft 365 allows for third-party (that's us 🐺) phishing simulations. Here's what you need to do:

1. Login to <https://security.microsoft.com/> with an account that has sufficient permissions (such as a Global Admin).
2. From the left menu, select **Email & Collaboration** > **Policies & rules** > **Threat Policies**.
3. From the new page, under Rules, select **Advanced delivery**.
4. Choose the **Phishing simulation** page, then **Edit**.
5. Add the campaign's Domain, AND the Sending IPs, as below.

    **Domain:** The phishing domain will be given to you by the consultant

    **Sending IPs:**
     - 199.255.192.0/22
     - 199.127.232.0/22
     - 54.240.0.0/18
     - 69.169.224.0/20
     - 23.249.208.0/20
     - 23.251.224.0/19
     - 76.223.176.0/20
     - 54.240.64.0/16
     - 76.223.128.0/19
     - 216.221.160.0/19

    _(We use AWS SES for sending email, so we can't control which specific IP will be used to send the email.)_

![Adding phishing simulation to M365](/assets/img/m365-phish-sim.png)

## Pre-launch: Testing the campaign

With your help, we will test the campaign end-to-end twice:

1. Once right after we've created and configured it; and
2. Right before the it goes live.

We'll send you an email and ask that you progress all the way through, entering fake credentials (or real ones; your choice).

If there are issues, we may need to delay launching the campaign.

## During the campaign

Observe how your staff react to the campaign, and write down some of their reactions. They could be fun to discuss later on. If you'd like, you can also let us know! We love hearing the results (read chaos) of our work.

## Post-campaign

After about 24 hours, most people would have had a chance to interact with the campaign somehow. Word of mouth spreads quickly and your more alert staff members have probably told others about the phishing attack (which is good). Time to call it a day.

With testing finished, it's important to **remove the allowlisting you added before.**

### Internal debrief

Social engineering is a sensitive topic. While computers don't mind being hacked, people can have a negative reaction to falling for a phishing campaign. It's vital that you reassure anyone who feels bad and show the campaign in a positive light, thanking people for participating in improving the company's security.

In our experience, shaming or reprimanding people who fell for the phish **makes your cyber security worse**. It stops people from reporting incidents and causes people to have a negative outlook on cyber security training. We don't want to work with companies who misuse our services like this and will refuse to perform further phishing exercises if we find any abuse.

An all-hands, open discussion about the campaign can be extremely beneficial. Ask (ahead of time) if people who were phished want to share their thought process for why it worked on them. Similarly, ask those who saw through the deception to explain what tipped them off.

Lastly, ask those who were phished to change their passwords. It's just good practice. 🙂
