---
title: Red team & Adversary simulation methodology
---

Volkis will perform a Red Team exercise on the organisation. Unlike a penetration test, the goal of a Red Team is not to identify vulnerabilities, but to achieve predefined goals. This is done by any means necessary whether it be through a computer or through social engineering. The Red Team continues executing campaigns until the goals are achieved, or the allotted time expires.

Our Red Team exercises are bespoke. As such, a flexible methodology is used in order to achieve better results and give us the ability to accurately simulate an adversary. We take industry standard frameworks such as CORIE, TIBER and, Mitre ATT&CK, into account but do not follow them strictly in favour of flexibility.

## Threat Modelling Workshop

Prior to commencing any campaigns, Volkis will conduct a threat modelling workshop with key stakeholders to answer the following questions:

- Who is likely to target the organisation?
- What goals would adversaries likely have?
- What level of resources do these adversaries posses?
- How long would these adversaries persist in achieving their goal?

These questions are discussed and the type of adversary to simulate is established. For example, this could be an insider threat or an opportunistic attacker. Secondly the following items are discussed and agreed upon:

- What is the **primary goal** of the Red Team?
- What are the **secondary goals** (if any)?
- How long will the Red Team last?
- Who is the primary contact in the target organisation?
- What is the communication process between the Red Team and contacts inside the target organisation?
- Are there any restrictions on the Red Team? (E.g. no physical intrusion)
- Should the Red Team trigger incident response on purpose after achieving their goals?

The **primary goal** is a business-level threat rather than a technical goal. For example, a primary goal could be to "steal the source code" of the organisation's core product.

**Secondary goals** can be either business-level or technical. For example, "prevent legitimate access to medical records" or "gain Domain Admin privilege".

Optionally, the Red Team can be given **threat cards** to play. A threat card can be played by the Red Team to trigger an event in the organisation. For example, a threat card may be "execute malware on a user's workstation" or "plug an implant into the internal network". Threat cards are useful for when there are restrictions on the Red Team, to progress the narrative when the Red Team is unable to progress towards their goal, or to better simulate an insider threat.

## Reconnaissance

The Red Team will perform reconnaissance and information gathering on the target organisation using both Open Source Intelligence (OSINT) sources and active scanning. In order to remain undetected, the Red Team will learn as much as possible about the organisation, their employees, and their business practices.

A list of security controls and products potentially used by the target organisation will be collected. If possible, the Red Team will create a lab environment with the same or similar tools in place to test custom malware prior to using them in the field.

If an insider threat is being simulated, the Red Team will request access to information that the adversary would likely know.

## Campaigns

Actions attempted by the Red Team while progressing toward their **primary goal** are divided into **campaigns**. The details of each campaign will depend on what adversary is being simulated and what the primary goal is. For example, a campaign may be "Malware dropper phishing attack" or "physical intrusion" or "lateral movement in the internal network".

The Red Team will have several potential campaigns that they could run. These campaigns are prioritised according to which one is likely to progress the Red Team towards their goals versus how likely it is to alert the Blue Team. Results gathered through successful and failed campaigns are fed back and the campaign priority may change.

### Preparation

Prior to executing a campaign, the Red Team gathers as much auxiliary information as possible to enhance their chances of success. Custom malware is written, devices are created, and infrastructure is spun up to assist the Red Team. A pretext is created for why this campaign exists in the first place. For example, it could be "a colleague sharing a file" or "an inspection of the HVAC system in the office".

Campaigns, as well as any tools and devices, are tested offline against security controls present in the target organisation to avoid detection. The campaign proceeds if detection is unlikely or if detection would not have an adverse affect on the Red Team.

### Execution

The campaign is executed and potentially adjusted on-the-fly to avoid detection. If successful, the Red Team will attempt to maintain their newly obtained level of access and leverage it in future campaigns.

If the campaign fails, the reasons are fed back to future campaigns and the Red Team may choose to repeat the campaign or move to a new one. A failed campaign may have triggered an alert for the Blue Team and may cause the Red Team to destroy their current infrastructure and rebuild. Execution of more campaigns might also be stopped for a time to allow the heat to die down.

The Red Team will continue executing campaigns until the goals are achieved or the allotted time-frame expires.

## Trigger Incident Response

One of the objectives of a Red Team exercise might be to test the incident response and forensics capability of the Blue Team in a simulated scenario that they are unaware of. If this is the case, and the Red Team have achieved their goals, a campaign will be executed to specifically get the Blue Team's attention. The Red Team will monitor the Blue Team's actions and see if they are ejected from the network and that implants are cleaned up.

## Red/Blue Debrief

After all campaigns are complete, Volkis will perform a debrief with the Red Team, Blue Team and key stakeholders. The entire Red Team exercise will be discussed from both teams' perspectives to gain insight into what each team observed. This helps both teams to learn and grow their skills, ultimately protecting the organisation from real-world threats.
