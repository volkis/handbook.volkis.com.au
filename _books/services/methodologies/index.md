---
title: Methodologies
permalink: /services/methodologies/
---

## Penetration testing methodologies

- [External penetration testing](/services/methodologies/external-penetration-test): Testing from the perspective of an internet facing attacker.
- [Internal penetration testing](/services/methodologies/internal-penetration-test): Testing from the perspective of an attacker with access to the internal network.
- [Web application penetration testing](/services/methodologies/web-application-penetration-test): Testing web applications or APIs.
- [Wireless penetration testing](/services/methodologies/wireless-penetration-test): Testing wireless networks, including enterprise and guest WiFi.
- [Mobile application penetration testing](/services/methodologies/mobile-penetration-test): Testing mobile applications for IOS or Android.

## Red Team Exercises

- [Red team and adversary simulation](/services/methodologies/red-team): Long-term targeting of an organisation with business relevant objectives. Usually includes social engineering techniques and physical intrusion.

## ACSC Maturity Model assessment

- [ACSC Essential 8 Maturity Model assessment](/services/methodologies/acsc-maturity-model-assessment)
