---
title: What value does Volkis as a company bring to penetration testing?
---

In building and running a cyber security consultancy, there's a question that always has to be front of mind for us: What value does Volkis itself provide?

This is more than just a bit of a boost for our egos. Rather, it's a genuine component of the consulting model - of the business model that Volkis fits into. Whenever we have an engagement, there are three parties that come into play. They are the client, the consultancy, and the consultants themselves. The value of the consultants' time is obvious: they perform the work. They find the security vulnerabilities in the systems that are being tested or find the ways the system might not be up to spec.

What is the value of Volkis? Why does a client not just directly deal with a consultant? The answer to this question can help guide Volkis to be a better cyber security consultancy.

## Connection between client and consultant

Let's first get the obvious out of the way. The company forms the main conduit between the client and consultant, including sales and account management. The connection is made, the scope is defined, the appropriate legal terms are defined and agreed, the project is managed, and payment is put through.

Unfortunately I feel like this first aspect is, for a lot of organisations that do penetration testing and cyber security consultancy, the only value they provide in the engagement. What else can we provide?

## Support from the team

Penetration testing and cyber security consulting is a hard job. A consultant might come across any of the technologies in the huge gamut of IT and suddenly be expected to learn enough about the technologies to be able to manipulate them. All of this technology is constantly changing in a way that makes it a version of the [Red Queen's race](https://en.wikipedia.org/wiki/Red_Queen%27s_race), where you spend all of your effort just trying to keep up. This is an imposing thought for anyone going into the field. To be able to cope you need to be able to lean on the rest of your team.

With a good team there's going to be other members who have experienced the technology or situation before. They can provide help, advice, or even active assistance. Team members can be used for brainstorming and ideas - just explaining your problem to someone rubber duck debugging style can help break through.

The scale of a team can provide access to tools that a consultant may not be able to have individually. Gaining access to testing infrastructure such as vulnerability assessment systems or reporting repositories and infrastructure can raise the efficiency, effectiveness, and capability of a consultant.

A well functioning and supportive team should make every single member more effective and productive.

## Training and mentoring

With the requirement to keep up with the newest techniques, security consultants need to be constantly training and learning. This is in addition to career development and all of the "soft skills" that can be improved. The consultancy should actively assist in the training of security consultants with a mixture of formal and informal training.

Formal training can be encouraged using a training budget and allocated time. While consultants can get external formal training separately from their work, the consultancy can provide direction and advice in training and match their training with client needs. Internal formal training can also help build consultant capability. With a capable team a consultancy can provide training such as learning days and internal events, shadowing, cross skilling, and peer review.

Informal training can come in the form of team support and mentoring. This can be encouraged by the consultancy by building a culture of learning and support.

## Capability

Again, penetration testing is a hard job and as part of this it's becoming more specialised. We're getting web app penetration testing specialists, network testing specialists, mobile app testers, smart contract testers, code reviewers, hardware specialists, and SCADA specialists. Add to this specialists in policies, process, governance, risk, and compliance, which regularly intersect with penetration testing requirements.

Sometimes a skilled generalist with the right support isn't enough for a piece of work. Instead, you need someone who is specialised in the work that needs to be done. As the cyber security requirements of organisations are becoming more complex, we can have organisations that regularly need multiple people with different specialisations even on a single project.

The consultancy needs to be able to match up the right people with the right skillset to the right jobs. When specialisations are required the consultancy should be able to match them up.

In addition to the capability internally at Volkis, we can leverage our partner network for our clients. Through our partners we can provide services for our clients such as incident response, managed services, security engineering, and IT services. Working closely with partners that share our ethics and commitment to quality allows us to deliver large interconnected projects.

## Capacity and coordination

Projects will never come in at orderly times and intervals. It's always incredibly lumpy, especially when end of the calendar or financial year comes along. There will be ten projects one week and no projects a few weeks after that. Projects might come in with tight deadlines requiring multiple people to work at once.

The consultancy needs to be able to manage all of this fuzziness in need and capacity, coordinating clients and consultants so that the work can all be done. To be effective in this, the consultancy must project manage and set expectations so that the consultants' time is being respected, with limited overtime and a healthy work-life balance. On the flip side, the consultancy can also help with low periods with long term coordination with clients and a steady job for the consultant.

Where multiple consultants are required, the consultancy can manage this, coordinating the consultants without extra effort from the client.

## Reputation and quality assurance

When a client engages a person or company to provide cyber security consulting, there is a lot of trust that the client has to place in the consultants. This could even include providing direct access to the internal network, permission to hack their systems, and even administrator credentials and direct access to the most sensitive information and systems the organisation has.

In addition to all this access that the organisation trusts will not be abused, there's the simple thought of "has the consultant done the job properly?" When there's only 5 vulnerabilities found, [it is hard for a client to know whether that's all there is or if there's another 25 hidden vulnerabilities that weren't found because the penetration test simply wasn't done well](https://www.volkis.com.au/blog/how-do-you-know-you-have-good-pentest/).

The reputation a good organisation builds can create trust in the client. That trust, when rewarded, builds the reputation of the organisation. This trust enables the tester to perform the test - without this the client would be simply unwilling to provide the necessary access and resources to engage.

After the reputation is built it can trancend personal relationships between the consultant and the client contact, spreading this trust to other members of the client including their technical staff, executives, and board members. The brand that the consultancy builds allows the consultant's work to carry more weight and create more action.

Key to building this reputation is consistent high quality of work. The way to provide consistency is to have strong quality assurance processes that act as both a safety net, catching potential mistakes, and a way to provide meaningful improvement to the output of testing.

At Volkis we also use transparency as a way of building our trust with clients and our reputation. It forms one of our key [Volkis Values](/company/volkis-values/). We have our open handbook complete with methodologies, guides, processes, policies, and procedures that our clients can freely browse. We also open source tools on our [Gitlab account](https://gitlab.com/volkis/) which also shows some of our work in improving security toolsets and processes. This "what you see is what you get" puts aside the uncertainty that acts as a barrier to building trust.

## Effective and efficient workflow and systems

The entire workflow of a cyber security consulting project includes the negotiation, intiation, engagement, reporting, debrief, and closure. Each of these phases can be assisted by effective systems backed with automation, standardisation, and an efficient workflow.

At Volkis we use automation to make sure the right people have the right data at the right times. This includes automation behind project coordination and utilises the secure cloud hosting of data. The cloud hosting also allows effective coordination when multiple consultants are on the same project, with each consultant having access to the same central data set.

Standardisation of processes and templates increases the efficiency of work. This includes report templates and vulnerability writeups. While such standardisation should not direct the output of reports, they can make consultants more efficient, saving time.

The effective and efficient workflow and systems allows the consultant to achieve greater consistency, more comprehensive testing, and a better result for the time allocated.

## Stability, HR, and additional employee benefits

Volkis as a company has a priority on hiring full time employees where possible rather than using contractors. Along with the legal protections this provides such as sick leave, annual leave, and ongoing stability of income, we look to provide additional personal and professional support to our employees.

The additional employee benefits are a mixture of formal and informal processes. The formal processes include events such as presentation days, conferences, dinners, and drinks, as well as a set of HR codes and policies that provide protection and direction where necessary. While these a great to have, the informal processes and support provide a greater impact on the health and wellbeing of employees. These include checkins, discussions, and the ongoing empathy that forms another of our core [Volkis Values](/company/volkis-values/).

## Risk management, legal protections, insurance, and limited liability

Offensive security is an inherently risky practice. Even when taking sensible precautions, systems can respond to scans in unexpected ways, operations can be impacted, and data can be lost or manipulated. Without proper protections, the consultant can potentially be personally liable for anything that goes wrong such as a server reacting badly to a scan or attempted exploit.

There are four parts to protecting the tester from a legal point of view:

- **Risk management**: The systems and processes involved should limit the likelihood and potential impacts of adverse events. This involves not just the right training, tools, and exploits, but also getting the right permissions for testing, making sure the right people are aware, and encouraging the client to take the right business continuity processes like backups where possible.
- **Legal protections**: Our terms and conditions holds clauses for the protection of Volkis and the tester. Explicit permission is provided to use offensive security techniques on the provided scope. The client is notified that the testing may not be completely comprehensive given the uncertainty of testing, with the report and results being provided as is. There is an indemnity clause preventing damages from being saught in the case of adverse events. There are also vulnerability disclosure policies that allow disclosing the vulnerability to third parties to get them fixed elsewhere.
- **Insurance**: We have professional indemnity, work health and safety, and product liability insurance. This will help cover us if the legal protections break down.
- **Limited liability**: Finally, Volkis is a limited liability company. This means that if Volkis is sued then the employees are, in most cases, do not have direct financial exposure.
