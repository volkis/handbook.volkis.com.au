---
title: Reporting process
---

For most projects at Volkis the main output is a report. For many of the people that may be affected by the testing, including techs who might be tasked with remediating vulnerabilities, executives who need to make sensible decisions based on risks we've raised, or project managers who need to know how this all might affect their rollout, the report will be the *only* exposure to Volkis they have. This means our reports have to be professional, informative, accurate, and concise.

This process does not cover how to write a good report (see other pages like the [Penetration Testing Engagement Guide](/services/pentest-engagement-guide/) for that). Instead it covers the process for creating a standard Volkis report.

This process should generally be followed for developing Volkis reports but **is not mandatory**. Like many of the guidelines and processes in this handbook, if doing something else will be better for the client then go ahead. Put everything in a spreadsheet. Draw a picture. Use smoke signals. As long as it's what's best for the client, your work is your work.

## Overview of Report Ranger

For Volkis reports we use Report Ranger, which is freely available at the Volkis Gitlab at the following URL:

- [https://gitlab.com/volkis/report-ranger](https://gitlab.com/volkis/report-ranger)

Report Ranger can programmatically put together your report, automating and simplifying a lot of the report generation process. It can do things like automatically put together risk assessments, grab information from spreadsheets, build vulnerability and data tables, sort and process vulnerabilities.

The Readme file should have up-to-date information about how to use the tool. More detailed information can be found in the [Report Ranger Wiki](https://gitlab.com/volkis/report-ranger/-/wikis/home).

## Empty project in the Sharepoint folder

After adding yourself to a Trello project, you should have access to the folder in Sharepoint containing the default empty project from the Volkis template. It needs the [Volkis template](https://gitlab.com/volkis/volkis-rr-template/) to run. See the Volkis template readme file for installation and running instructions.

If you are doing whitelabel work, you may need to use a template that is not Volkis. They are most likely going to be in the Volkis template Gitlab directory. If they are not there they will be available in the Volkis gitlab, usually with the name `Company-RR-Template`. While most templates use the same file structure as the Volkis template (and, in fact, just reference the Volkis template with minor modifications to look and feel), there may be templates that may have slightly different content to the Volkis template. In this case you should use the template's `Empty Project` folder instead of what's in there by default.

The readme file of each template will describe how to use the template within Report Ranger.

The empty project will have the base files for building a report. If it's the first time using Report Ranger it's worth running it on the empty project to see how it generates.

## Pentest docs

We have a repository of pre-written files which can be used in Report Ranger reports in our [Pentest Docs Gitlab](https://gitlab.com/volkis/pentest-docs).

When looking at the empty project, you'll see directories like "Additional recommendations", "Effective security practices", "Root causes", and "vulnerabilities". These match up with directories in Pentest Docs and the files from there can be copy pasted right into your report.

Just because they can be copy and pasted in though doesn't mean they're necessarily finished. You will need to read through everything you copy and paste into your report to make sure they're accurate for the context you're placing them in. Most vulnerability templates in particular will need at least information about the system you're testing. These often use the headers of the file.

## Finalising the report and sending off to the client (the mandatory stuff)

I know at the start we said the reporting process is not mandatory, but you should know what **is** mandatory:

- Any reporting must fulfil the scope of the proposal. While the scope is generally written to provide flexibility it will still provide expectations on the basic information that should be in the report.
- All files must go through our quality assurance process prior to being sent off.
- The files must be securely sent to the client.

