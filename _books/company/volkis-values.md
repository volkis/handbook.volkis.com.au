---
title: Volkis values
---

**Whether they are staff, friends, family, or clients, the ultimate goal of Volkis is to help the people around us by keeping them safe.**

To support this goal, we aim to follow six values:

- Empathy
- Learn and grow
- Supporting each other
- Giving back to the community
- Transparency
- Independence and ethics

These values will support our growing team and make sure that we keep what makes Volkis special.

## Empathy

We have empathy for each other and empathy for our clients.

Volkis is made up of people who are trying their best to live a happy and fulfilling life. We must take care of our people so they can thrive. People who thrive will naturally do better, are happier, work smarter and it is truly a wonderful thing when that happens. We want them to feel safe with us, like someone has their back, not just with work but also in life.

When working with people we should always assume positive intent. We should give the benefit of the doubt and assume that information is provided in good faith.

Volkis is an all-remote company. When done well, this should enable a greater work-life balance. We do not manage hours or require a routine 9-5 day but instead seek results from employees.

No company is secure just for the sake of being secure. They're secure to enable their business to do what they do. To help our clients, we need to understand and empathise with them so that we can help them achieve their objectives. We want to do what we love so they can continue doing what they love.

In this, we provide a bespoke service. We don't try and do checkbox or cookie cutter work. Instead, we try and understand what the organisation is trying to achieve and help them get there. This includes doing what's best for the client instead of our generic guidelines if the guidelines don't suit the client's circumstances. For instance, a report may be better sent as a spreadsheet or uploaded into the client's ITSM system. The headings and information contained can all be changed to suit the client's circumstances.

## Learn and grow

Everyone is encouraged to explore, research, learn, innovate, and develop their skills. We're constantly trying to improve and find new ways of doing things.

Learning, both formally through training courses and certifications as well as informally through mentoring and peer learning, should be encouraged and considered as a key part of our processes. Training is assisted with training budgets and time allocations.

Nothing is sacred at Volkis - if there is a better way of doing what we do, then we should do it that way. People should feel safe to ask and answer questions and query what seems to be foundational processes. There is no "that's just how we've always done it". This allows us all to improve.

To grow, we need to seek diverse perspectives and embrace our differences rather than seek to minimise them. The different perspectives can help us all learn, gain insight, and be a better team.

## Supporting each other

Cyber security consultancy is a difficult job. No one person can know everything about all the systems and businesses we might come across. To be effective, a consultant must be able to lean on the full capabilities of the team and the organisation.

We need to support each other and help each other by answering questions, sharing knowledge, and assisting when we can so we can all succeed. There should be no egos at Volkis that limit the sharing of knowledge or building of skill.

With support, there must be respect. We must respect everyone and their expertise regardless of seniority or tenure.

Part of the support people need is keeping a sensible work-life balance. We need to be careful to avoid burning people out or being a negative impact on the lives of the people who work here. We do this by sharing workload and minimising overtime where possible.

## Giving back to the community

We look to support and build the cyber security community both personally through mentoring, development, and sharing of knowledge, and as an organisation through sponsorships, transparency, and swag.

Although we are a close team, we must understand we are part of a greater community. Volkis should seek to support the community just as the community has supported us.

This means having direct involvement with members of the community outside of Volkis. We support community members through sharing knowledge as if they are team members through mentoring and development.

Volkis employees should be encouraged to present talks, release tools, run workshops, and build on the ever-growing public knowledge. This will be actively supported by the organisation through budget and time allocation for community work.

## Transparency

We need to be honest and transparent with ourselves, our clients, and the wider community.

A philosophy at Volkis is to be "open but secure". Unless there is a reason for the information to remain private, such as it being a security risk or being protected by NDAs, we will, by default, make that information public. In this, we will provide information about how we do things to our current and potential clients and to the public.

## Independence and ethics

Volkis is an independent cyber security consultancy that works to provide unbiased services and advice. We will work to protect our independence and integrity and resist outside influences that would compromise that integrity.

Independence is enforced through our [policies and processes](/services/policies/independence-policy/). We need to see our position of independence as more important than partner or client relationships.

We need to be ethical in all of our dealings with ourselves, clients, and the wider community. Our clients put a lot of trust in us and we need to make sure we are worthy of that trust.