---
title: Code of ethics
owner: Matthew Strahan
last_reviewed_on: 2023-02-21
---

Volkis seeks to ensure that we act fairly, with honesty, integrity, and high standards of respect, following relevant laws and regulations. The work that we do relies on the trust of our customers and the wider community and by acting ethically we can maintain this trust.

This code of ethics provides the ethical standard we follow and is mandatory for all employees and contractors of Volkis to follow at all times.

## Honesty and integrity

- You must act with a high standard of honesty and integrity.
- In your position, you may have access to confidential and privileged information. You must maintain confidentiality over this information and must not use this information for personal gain.
- You must honestly represent your abilities and capabilities as well as the capabilities of Volkis. This includes ensuring that skills and knowledge is maintained to ensure that your abilities are up-to-date.
- Reports and communications to clients and partners must be professional and accurate.
- There will be no tolerance of retaliation of any kind against a person making a report or complaint of a violation of this code of ethics or of illegal activity. Similarly, there will be no tolerance of retaliation of any kind against a person cooperating in an investigation of a violation of this code of ethics or of illegal activity.

## Responsibility over property and finances

- You must have care when using Volkis property to prevent damage and to maintain its security. This includes user devices, testing devices, digital assets, and other Volkis property you hold or have access to. You must report any theft, damage, or security incidents involving Volkis property.
- You are expected to be responsible when submitting business expenses. When you submit an expense, you must make sure that the cost is reasonable, directly related to our business, supported by documentation, and in compliance with the Volkis [expense policy](/operations/expenses/).
- You must report any potential financial misconduct that you encounter in your employment at Volkis. This could include embezzlement, money laundering, or other financial misconduct or crime.

## Conflicts of interest

- You must abide by the Volkis [independence policy](/company/policies/independence-policy/).

## Legal and regulatory compliance 

- You must comply with all applicable government laws and regulations as part of Volkis.

## Business dealings

- You must not promise or offer bribes or unethical inducements in order to gain personal or business benefit. This includes attempts at extortion.
- You must not engage in activities that would consitute an unfair restraint of trade, unfair trade practice, or anti-competitive behaviour.

## Volkis ethics

- Volkis must not directly or indirectly participate in any form of forced or compulsory labour. This includes work or services coerced under threat, force, or penalty. This includes indentured or bonded labour, slavery, servitude, or other slavery-like circumstances.
- Volkis must not require workers, as a condition of employment, to surrender their passport, government identification, or any other document required for free movement.
- Volkis must allow all workers to freely terminate or leave their employment (outside of permitted notice periods)
- Volkis must not participate directly or indirectly in any form of human trafficking. This includes the recruitment, transportation, transfer, harbouring, or receipt of persons under any sort of threat, force, coercion, abduction, fraud, deception, or abuse of power.
- Volkis must not employ child labour, or any persons below the minimum working age of either Australian law or the local jurisdiction.
- Volkis must uphold the rights of employees for freedom of association, collective bargaining, and the right to join and form trade unions.


