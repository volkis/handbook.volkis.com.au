---
title: State of Volkis, January 2022
---

This document contains the findings of the Strategy Session in Volkis. It shows how we see ourselves, what we do well, what we can improve, pain points, and what we feel we currently stand for.

## What does Volkis do well and should keep doing?

- **Communication**: Volkis currently communicates well with each other and with clients. Issues get raised quickly and concerns are addressed.
- **Culture**: Volkis has a culture of fun, support, trust, learning, and quality.
- **People and support**: Volkis has really good people. Along with this Volkis takes care of employees with training, benefits, equipment, learning, and awesome Christmas parties.
- **Personal responsibility**: The consultants at Volkis are empowered to make decisions that are best for the client. There is implicit trust at Volkis which is refreshing.
- **Solving client problems**: The consultants at Volkis are interested in working with clients to improve their security, finding opportunities to improve the results and outcomes. The presentation of reports in particular is targeted for the client's unique situation, instead of just providing 400 page reports of rubbish.
- **Tranparency**: Volkis publishes information including methodologies, guidelines, processes, and information to our handbook and blog. The handbook is referenced by other people in the industry and also allows clients to properly know what they're getting.
- **Efficiency**: Volkis has a nice level of automation which helps people do their work without interference. While there is automation, there's also the freedom and flexibility to change the results and consultants don't feel constrained by the automation.

## What can Volkis improve?

- **Improve process and documentation**: Currently there's still many parts of the organisation that aren't sufficiently documented and to get answers you have to ask people. While we don't need a rigid process, better guidelines and manuals would be beneficial especially for new starters.
- **Feedback mechanisms**: While we used to have a feedback mechanism, it wasn't working well. We need a feedback mechanism that can get information from clients including through partners.
- **Fair QA process**: QA for proposals and reports is not currently assigned in a fair manner. It's more whoever might have time. We have the opportunity to put in a round robin or similar feedback process.
- **Engaging outside of the security community**: We do well engaging within the security community but there is a lot of opportunity to engage with IT and business communities. This could include presentations, videos, blog posts, and formal marketing.
- **Marketing**: We were doing videos and blog posts regularly but this fell to the side when things got really busy. It would be good to make time and pick this up again.

## What are pain points that could be taken away?

- **It's difficult to update pentest docs**: We have the Pentest Docs Git repository, but people aren't consistently keeping it up to date. The process for updating it gets in the way of the job.
- **Difficult working with partner and client communication platforms**: It is particularly difficult to handle accounts in partners and clients with unique email addresses and Microsoft Teams accounts for multiple organisations. Could there be support or a process to assist with this?
- **Password cracking machine**: We need to speed up our password cracking. (Note from Matt: I feel they could be angling to try and get some 3090s for personal computers)
- **External scanning solution**: We currently do not have a great solution for external scanning, with people using their laptops and home connections.

## What should Volkis stand for?

- **Fun**: We enjoy ourselves, do cool shit, and be the unique company that we are.
- **Empathy**: We have empathy for each other and empathy for our clients. We don't just do cookie cutter penetration testing. Instead we try and understand what the organisation is trying to achieve and help them get there.
- **Giving back to the community**: We look to support and build the cyber security community both personally through mentoring, development, and sharing of knowledge, and as an organisation through sponsorships, transparency, and swag.
- **Independent**: Volkis is an independent cyber security consultancy that works to provide unbiased services and advice. We will work to protect our independence and integrity and resist outside influences.
- **Push the boundaries**: Everyone is encouraged to explore, research, learn, innovate, and develop their skills. We're constantly trying to improve and find new ways of doing things. Nothing is sacred - if we have a better way of doing what we do then we should do it that way.
- **Supporting each other**: Cyber security consultancy is a difficult job and no one person can know everything about all the systems and businesses we might come across. We need to support each other and help each other by answering questions, sharing knowledge, and assisting when we can so we can all succeed.