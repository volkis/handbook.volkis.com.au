---
title: State of Volkis 2024
owner: Matthew Strahan
last_reviewed_on: 2024-04-10
---

This document contains the findings of the Strategy Session in Volkis. It shows how we see ourselves, what we do well, what we can improve, pain points, and what we feel we currently stand for.

## What does Volkis do well and should keep doing?

- **Communication**: We communicate well with clients and internally. This is not just during the tests but we have improved on our communication with clients before projects and between projects.
- **Reporting**: Our reporting is good quality while being efficient. The new Typst based report flow is working and the automation in the background helps our efficiency.
- **Training**: We take training seriously and this is reflected by time and budget being allocated. Preso days are an asset to the company and the new format where not everyone presents every time works well.
- **Personal responsibility**: Consultants are provided the ability to make their own decisions when it comes to the delivery of work and what could be best for the client. Consultants get "treated like adults".
- **Flexibility and remote working**: We can move around our working hours to accommodate life stuff.
- **Support**: Consultants get support from each other. 


## What can Volkis improve?

- **Scheduling of red team services**: We are having difficulty performing red teams alongside normal work. We need to experiment on whether to have consultants full time on red teams or potentially having a more fixed project plan.
- **Defining and minimising "sources of truth"**: We've currently got a bunch of systems where information is held. It's hard to identify what is correct and where to look for what.
- **Blog posts**: We don't release blog posts nearly as much as we did before.
- **End-to-end process for clients**: From a client's perspective there's still some activities that feel disconnected. The presales engagement in particular isn't quite as connected as it could be.
- **Cool stuff to do**: As we've grown, we haven't done as much hackery stuff as before. We could be doing a group research project, hacking on hardware or even doing CTFs together.

## What are pain points that could be taken away?

- **Frequency of meeting requests**: Donut/Playdate is frequent enough that it's become irritating. We should turn the number of requests down.
- **Presales handover meetings**: The handover meetings that have been implemented currently don't work effectively. They could be more targeted or skipped if they're not necessary.
- **Knowing who has licenses**: Especially Dehashed. The current way is just to yell in Slack.

## What should we keep doing?

- **Being disruptive and honest**: We have the opportunity to say "that's not how the industry should be". We can make noise and be a little louder.
- **Community engagement**: We sponsor and support community events including conferences, training sessions, and student societies.

## What does Volkis stand for?

- Reputable work.
- Just because it's always been done this way doesn't mean it should.
- Giving people an alternative to the big chain pentesting companies. The opposite of corporate.
- We care about each other, our clients, and the community.
- Helping everyone around us thrive. That includes our loved ones, our community, our clients, and our clients' customers who trust our clients with their data.
