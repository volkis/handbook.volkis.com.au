---
title: State of Volkis, February 2023
owner: Matthew Strahan
last_reviewed_on: 2023-02-28
---

This document contains the findings of the Strategy Session in Volkis in February, 2023 which involves input from all of the Volkis team. It shows how we see ourselves, what we do well, what we can improve, pain points, and what we feel we currently stand for.

This is a follow up to the [previous State of Volkis](/company/state-of-volkis-jan-2022/) held in January 2022.

# What does Volkis do well and should keep doing?

- **Training and learning**: Volkis values training and learning as part of the company culture and backs it up with training budgets and a simple approval process that trusts that the consultant can make their own decisions about what training is best.
- **Company culture**: There are processes that make people feel more like part of a team, including preso days, dinners, and catch ups. Everyone is open to helping each other and finding issues.
- **Flexibility**: Consultants can choose their own schedule. This reflects in people being happier and healthier. Even during sickness or disruption, people feel confident in the support that is offered.
- **Client engagement during projects**: We don't just deliver a service to the client. Instead we look out for their best interests, even doing extra work if it gets them a good result. We attempt to figure out what they care about instead of just finding vulnerabilities and saying "here's an RCE". Penetration testers feel empowered to make choices that are best for the client.
- **Reports**: Feedback from clients, partners, and new consultants is that our reports provide more value and have better presentation than similar reports from other companies.
- **Improving processes**: Onboarding process had great feedback. We have made initial improvements to our scheduling and project coordination.
- **Community engagement**: We give back to the community, including sponsorships, education, open sourcing tools, and having consultants perform work that benefits the community.
- **Marketing**: The social media and merch is on point. The shirt designs have been really well received. People have been approached at conferences to complement social media posts. 

# What can Volkis improve?

- **Scheduling and the "in-between"**: In the last half-a-year we had low efficiency and our billability did not match what we were feeling. The consultants are spending significant time chasing information. Jobs are being pushed back due to not having the requirements. We need to improve the "in-between" time that connects presales and the consultant doing the work.
- **Scoping**: We are underscoping work. The statistics show that we often exceed the time allocation for work. We give enough time for the testing and reporting, but we need to also include time for project coordination, QA, communications, debrief, and retesting.
- **Long-term client engagement**: With long-term contracts and master services agreements being signed, we need to improve the processes for ensuring ongoing project coordination and reporting. We also need to improve our ongoing client support, having regular catch ups.
- **Feedback from jobs**: Our feedback mechanisms are not yet solidified. Our previous survey has not been sent to all clients and we don't have feedback mechanisms from the client to the consultant or from the consultant to presales.

# What are pain points that could be taken away?

- **Report Ranger polishing**: Report Ranger still suits our purposes but it needs a lot of love. It's 80% of the way there and the last 20% will make life a lot easier for us. In particular we are using it for more than just penetration testing reports. This means it needs to be a report builder not a pentest report builder. Also, if Matt's not available and something goes wrong that causes delays.
- **Pentest docs**: Our vulnerability template process is much better than last year but it can still use work. Finding the right vulnerability is a pain and there's a bunch of cleaning and redoing that needs to be done. In particular changes to RR tie into changes to pentest docs.
- **Phishing infrastructure**: We currently rebuild phishing infrastructure for each test. This is difficult and leads to error. There is potential to be more efficient and automate the infrastructure build process. If automation isn't feasible we should at least document it all.

# Do our values still match up the reality?

Last year we took everyone's input and built [Volkis Values](/company/volkis-values/). The consensus of the team is that these values both match up to what we would like to be and what we are. While the values themselves don't need to be changed, the specifics underneath could use some work and updating, in particular linking with the work that has been done over the past year.
