---
title: Supplier Management Policy
owner: Nathan 
last_reviewed_on: 2024-09-19
---

All aspects of services supplied by Volkis will have some third-party involvement. From sending proposals, to project management, to storing archived project data. Each of these needs has a different set of requirements when choosing an appropriate supplier. As such, before a supplier is chosen the requirements need to be identified. This is based on the following criteria:

- What problem are we trying to solve?
- Is the data going to be shared with a client?
- In what direction is the data served? I.e is this a service we use to extract data from? Or does Volkis need to use it to host data?
- Is the data sensitive?
- Who is going to use it?
- Is this a once-off requirement, or ongoing?

After the requirements have been established, the search for an appropriate supplier can begin. When choosing a supplier, the following should be considered:

- Does an existing supplier provide this service?
- Who are the competitors in the market?
- What is their reputation?
- Are their policies public? E.g. data handling and protection policies.
- Does the supplier have mechanisms that integrate with existing workflows and suppliers. E.g. Automation and authentication tools. 

When an appropriate supplier is identified, a risk assessment needs to be completed. 

## Risk assessment

A risk assessment of a potential supplier involves an analysis of their policies and procedures, and determining whether they align with Volkis's requirements. Security requirements should be considered proportionally to the level of risk associated with the business needs.

### Confidentiality, integrity, and availability of information.

The supplier must have appropriate policies and procedures to ensure Volkis's data is protected and that it cannot be viewed or altered by a third-party. This is critical to ensure that Volkis's client information remains confidential.

### Data protection

Volkis often deals with clients who have strict requirements for data storage, including requirements related to data sovereignty and compliance with relevant laws (such as the Privacy Act 1988). If the requirements state that the data is of a sensitive nature, the supplier should have policies and information pertaining to how data is stored and protected. 

The following should be evaluated:

- Data ownership and control.
- Data encryption, both at rest and in transit.
- Backup and disaster recovery procedures.
- Conditions and processes for data retrieval and secure deletion upon termination of the service.

Volkis must ensure that all data held or processed by suppliers complies with applicable data sovereignty laws and regulations. Suppliers who handle or store data must provide transparency regarding the location of data storage and processing, ensuring compliance with Australian laws.

Additionally, the supplier should have clear policies on how any collected data may be used or sold. 

### Incident management and reporting.

The supplier should have clear procedures about how they handle incidents and policies on client communication. In the event of an incident, Volkis should be made aware of:

- What happened 
- The steps, if any, Volkis needs to complete to protect the business
- What actions are being performed by the supplier to remediate the issue

### Supply chain

If the supplier utilises third-party suppliers themselves, the security posture of those suppliers must also be considered. Suppliers must be assessed for their ability to manage security within their own supply chain, ensuring that security standards are maintained through all tiers of the supply chain.

### Access control

Volkis employs the principle of least privilege wherever possible. All Volkis employees are granted only the level of permission required to perform their role for each service. The supplier must provide access controls to restrict access.

For cloud environments that have a web interface and login functionality, the supplier must support multi-factor authentication. Ideally, the supplier should also support single sign-on.

## Onboarding

When a supplier has been appropriately vetted, Volkis may choose to engage with the supplier. If the supplier is only a short-term solution, many of the below actions may not be required. However, for long-term solutions, the following must occur:

### Seeking approval from management

Before engaging with a supplier, approval must be sought from management. This will involve discussing the needs of the business and the solution the supplier may offer. 

Who will be using the supplier, why the supplier was chosen, and appropriate levels of licensing will be discussed.

### Configuration of access controls and security controls

Once engaged, appropriate security controls must be configured before Volkis data can be committed to the supplier. This includes configuration of access controls, and integration with SSO provider.

### Licensing

Licenses or subscriptions must be added to the Asset register ("Volkis Ops - Documents\Operations\Asset Register.xlsx") to ensure they are tracked appropriately and reviewed annually. 

### Handbook

A short playbook of how to use the supplier must be published in Volkis's handbook for all employees to access. This is to ensure all staff understand how and why the supplier is being used, and in what situations they may need to interact with the service. 

Generally, this playbook will walk the user through performing common tasks and contain links to documentation where more detail is required. 

The supplier must also be added to the list of third party providers in the handbook.


### Employee onboarding and off-boarding

Configuration of user accounts and access controls must be added to the employee onboarding and off-boarding procedures. This ensures that Volkis employees have appropriate access to the supplier when it is required, and also that access is appropriately revoked when an employee leaves Volkis. 

## Monitoring

Volkis will regularly monitor, review, and evaluate supplier information security practices and service delivery. This includes monitoring social media and information security feeds. Should an incident occur with a supplier, Volkis will independently determine whether an incident response is required. This may include temporarily disabling the service if possible. 

Supplier performance and security compliance must be reviewed at least annually or when significant changes occur in the supplier’s operations, services, or security posture. Relevant documentation will be reviewed when updated to determine if the changes have a direct impact on Volkis.

Changes to supplier security practices must be managed to ensure any impact on Volkis’s security posture is identified and addressed.

## Management

To ensure Volkis does not over-commit to a individual supplier, licences, subscriptions, and usage for the supplier's services will be reviewed annually. This ensures that Volkis has enough of the supplier to reliably provide service, while not committing to services that are not required for the business's needs. 

As changes are made to the supplier's licencing and subscription models, Volkis may opt to increase or decrease the tier of license purchased. Or, if necessary, change to a new supplier. 

If the supplier is being used more than intended, an internal review may take place to determine the root cause. Volkis may employ systems to prevent over-use of the supplier if necessary. 