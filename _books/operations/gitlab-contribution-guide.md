---
title: Development Guidelines
owner: Finn
last_reviewed_on: 2024-08-05
---

Volkis utilises GitLab for a number of external and internal projects. Keeping all of these projects running smoothly and collaboratively is important. This page establishes clear and consistent processes for how Volkis' employees can contribute and collaborate on GitLab.

## When committing

### Concise, clear messages

Having concise, clear commit messages can help other developers understand what changes you have made quickly.

### Commit often

Committing often in git allows you to quickly roll-back changes. Since git keeps track of all changes in files you don't have to worry about losing code that's in a previous commit.

### Rebasing

Sometimes, you might have a list of 5 or 10 commits that show different attempts and changes made to a single file that you don't need to keep track of. Check out `git rebase -i`, specifically the squash command, and how that can allow you to compress multiple commits down into a single one.

## When pushing

### Public vs Private Repositories

Anytime you push a piece of code from your local computer to a git repository you should be cognisant of the visibility of that project.

Git keeps track of all changes between files, removing sensitive information isn't as simple as creating a new commit that deletes the offending content. Looking in the git log will reveal the sensitive information in all it's glory:

```diff
-API_KEY = 'supersecretapikey'
+API_KEY = os.environ['API_KEY']
```

If you do accidentally push a secret key or information into a repository, then you will need to remove that from the repository using a tool such as [BFG](https://rtyley.github.io/bfg-repo-cleaner/).

Some examples of our public repositories:

 - The Volkis Handbook
 - Volkis client scripts

Some examples of our private repositories:

 - The Volkis main website
 - The Volkis private Handbook (ssshh!)

## When creating a Merge Request

### Testing functionality

Before creating a Merge Request, you should ensure that the code you are committing functions in the way you intended, and complies with any style guides the project encourages. For some of our projects, specifically the handbook, you can also see the changes reflected real-time once the Merge Request is open, which allows you to double-check the correctness before you ask someone to come and review your changes.

### Avoid grouping too much

"Aim to create small, focused \[merge\] requests that fulfil a single purpose" [^1]. 

Having small merge requests allows you to iteratively add features into a project, without the worry of some functionality being unimplemented. Additionally, it helps others look back and understand the changes made to the project over time.

### Provide clarity

Merge Requests (MRs) should have easy to understand titles and descriptions, so that the reviewer can quickly triage and understand what your merge request does. Ideally, the description should include:

- Why your MR exists
- A high-level overview of the changes to the project
- Links to any relevant issues

You can additionally provide details on what feedback you want from the reviewer. For example, for some MRs you may want a full, in-depth check of the content or code, as opposed to a quick functionality, spelling and grammar check.

## When reviewing a Merge Request

### Provide helpful, actionable comments

When reviewing Merge Requests (MRs), it is best to spend a little bit of time and provide helpful feedback. Remember, MRs can be the result of hours of work on a developers end, and should be treated with patience and respect.

With this in mind, avoid comments like:

> This doesn't look right

Instead, include more details such as:

> The functionality here would fall-over in x, y and z use-case.

If possible, use the interactive comment functionality that allows you go to through and comment on specific lines in the code. This process allows the reviewer to provide comments that have direct context, and decreases the friction in communication.

### Provide a reason for closing a Merge Request

Providing a reason for closing a merge request helps both current and future developers understand why a specific change is deemed unnecessary or redundant.

### Check for sensitive information

If this a merge request for a public repository, it's important you double check the code to ensure that no sensitive information is present.

## When creating a project

### Tools to better enable collaboration

A formatter can allow you to enforce a specific style for the code, and have all the other developers on a project follow suit. Take a look at these options:

 - [prettier]( https://prettier.io/ ) for JavaScript and TypeScript
 - [ruff]( https://docs.astral.sh/ruff/ ) for Python
 - [gofmt]( https://pkg.go.dev/cmd/gofmt ) for Golang
 - [rustfmt]( https://github.com/rust-lang/rustfmt ) for Rust

Issues are a great way of keeping track of tasks and items that still need to be completed, and can easily be linked to from Merge Requests.

[^1]: https://docs.github.com/en/pull-requests/collaborating-with-pull-requests/getting-started/best-practices-for-pull-requests

### Ensure that you consider security

When creating a new project make sure you consider the information security implications of that project. You should consider:

- Is there sensitive information in the project? If so it should be Private.
