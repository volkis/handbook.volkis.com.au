---
title: Volkis style guide
---

## Introduction

The Volkis style guide is a constant work in progress. It covers all documents including reports, web sites, presentations, and handbook entries.

Volkis uses Australian English at all times.

## Naming convention

Report filenames should be in the following format:

```Volkis report - \{\{ client }} - \{\{ title }} \{\{ version }}```

The version can be ommitted if the version number is 1.0.

When a date is used in a filename, the date should use YYYY-MM-DD, for example 2024-09-02 for 2nd of September, 2024.

## Versions

Volkis reports and documents should use the ```major.minor``` version numbering system.

## Capitalisation

Titles should be capitalised in sentence case. For example:

* External penetration testing report
* Attack walkthrough
* Volkis style guide

Capitalisation should be limited to the first letter of each sentence and proper nouns.

## Emotive language

Most documents that are released by Volkis are technical documents that attempt to convey information in a concise and informative way. In technical documentation we should avoid emotive language. The application isn't suffering from anything. The vulnerability isn't extremely or enourmously important.

We need to be factual and "matter of fact" when describing our findings. This might make it sound less fun but it avoids overexaggerations and being misconstrued.

This doesn't apply to blog posts and irreverent marketing. Go ahead and use as much emotive language as you want in those cases.

## Dates and times

Although dd/mm/yyyy is preferred in Australia, it is ambiguous for overseas readers. For this reason, it is preferred to use yyyy-mm-dd hh-ss format.

## Screenshots and images

All screenshots within a report should have captions describing the screenshots.

## Source code

In markdown, all source code and computing variables should be encapsulated within triple ticks (\`\`\`), even if they are part of a sentence. This should include:

* Variable names
* Function names
* IP addresses
* Email addresses
* Filenames
* File paths

Items that do **not** need to use code blocks include:

* Document names
* Product names
* Port numbers
* Line numbers
* References (including CVE numbers, RFCs, etc)

There are cases where you might use your discretion, depending on context:

* Error messages
* Usernames
* Comments within code

## Specific standards and frameworks

This section covers wording and phrases for specific standards and frameworks that are commonly used.

### Essential 8

The Essential 8 Maturity Model refers to a baseline of 8 strategies for mitigating real-world cyber security attacks. It is based on the ASD's experience in responding to attacks and is regularly updated by the ASD.

- **Maturity Level 1/2/3**: Capitals and numerals should be used when referring to a specific maturity level (such as Maturity Level 1, Maturity Level 2, and Maturity Level 3). When you are not referring to a specific maturity level, lower case should be used.
- **Avoid using the term Maturity Level 0**: This term is no longer used in the Essential 8. Instead say "Does not meet the requirements for Maturity Level 1".
- **The Essential 8 is released by the Australian Signals Directorate (ASD)**: As of 2024, **avoid** saying it is released by the Australian Cyber Security Centre (ACSC)

The Essential 8 Maturity Model heirarchy should be referred as follows:

- The Essential 8 Maturity Model Framework describes eight **strategies**. These strategies are:
  - Patch applications
  - Patch operating systems
  - Multi-factor authentication
  - Restrict administrative privileges
  - Application control
  - Restrict Microsoft Office macros
  - User application hardening
  - Regular backups
- Each strategy has up to three **maturity levels**, referred to as Maturity Level 1, Maturity Level 2, Maturity Level 3.
- Each **maturity level** has a number of **requirements**. (Except Maturity Level 2 for Patch Operating Systems but that's another story)
- Each **requirement** can be met by a **control**.

Try not to get the terms mixed up.

## Specific terms

This section provides directions for specific terms that may have unique meaning, capitalisation, or phrasing.

### And/or

Avoid using slashes such as "and/or", "accept/enforce", etc. Instead, the sentence can be rephrased to remove and clarify it. Mostly this would be replacing "and/or" with a simple "and" or "or".

### A number of

Avoid using "A number of". Instead, either say the specific number or remove the phrase.

### Brute force

Use "**brute-force**" over "**brute force**" or "**brute forcing**". This includes:

* Brute-force attacks
* Brute-force techniques
* Brute-force search

### LaTeX

Document processing system used in Report Ranger. Pronounced "LAH-tekh". LaTeX should have the appropriate capitalisation. Use "\\latex" to format this correctly when using latex.