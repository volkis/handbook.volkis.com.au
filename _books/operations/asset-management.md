---
title: Asset Management Policy
owner: Nathan 
last_reviewed_on: 2024-08-05
---

## TLDR

Key assets, including laptops, shared devices, and software need to be registered in the Asset Register spreadsheet. Peripherals and small purchases (hacking gadgets, upgrades, etc) do not need to be included in the register. 

You can find the spreadsheet here:

- "Volkis Ops - Documents\Operations\Asset Register.xlsx"

If you need to send the asset somewhere, you need to tell project management so they can update the spreadsheet. We will do quarterly reviews of where assets are.

## What is the purpose of Asset Management?

While Volkis may not have as many assets as a larger organisation, we do have some, and we rely on these assets to do our work. Assets, in this context, refers to any software or hardware purchased by the company so we can perform our respective jobs. This includes laptops, tools, software licences and subscriptions, etc. 

Management of these assets allows for more informed budgeting decisions and ensures the tools we have at our disposal are fit for purpose. It also allows us to track their location or allocation, so we know where they are and how we can get them to the next person who needs them.

We have some tools that are shared among all the consultants and will travel from person to person depending on the jobs they are working on. We need to know where these items are so they can be transported to the next person. For example, we have a kit full of tools used for physical intrusion tests. Given that we don't perform these tests weekly, we only need one (or two) sets of tools across the whole company at any given time. However, without proper management of the asset, it can be easy to lose track of the kit and send everyone into a panic trying to find it. 

## Which assets need to be managed?

There is no hard-and-fast rule as to which assets need to be managed specifically, but it largely comes down to the purpose and value of the asset. It can be valuable to know that we have an extra laptop on hand and in the event a consultant's machine dies and we can over-night ship it to them to use and continue working while they order a new one. 

Some examples of assets we need to track are:

- Consultant laptops
- Shared devices (Volkbox, spare laptops, iPhone, Android phone, etc)
- Physical Intrusion Tool Kit
- Software licences (Nessus, Burp Suite, Cobalt Strike, etc)
- SaaS providers (M365, Trello, Resource Guru, etc)

However, not everything we purchase needs to be tracked. Generally speaking peripherals, upgrades, and gadgets do not need to be tracked. Some examples:

- Monitors, Keyboards, Mice
- Microphones, headsets
- Adaptors, Cables, Chargers
- Hacking gadgets (Flipper Zero, etc)
- System upgrades (memory, storage, etc)

## Ensuring assets are fit for purpose

By having an up-to-date register of assets, we can make informed decisions about which assets are worth the continued investment, which need to be replaced, and which can be retired. 

In the case of computer hardware, laptops will age, their warranties will expire, they become slow and inefficient. As such they need to be replaced periodically. Having recorded purchase date and approximate specifications allows us to allocate appropriate funds to its replacement when the time comes. If we know that certain vendors or models age more quickly than others, we may avoid using those vendors in the future. 

In regards to software, some subscriptions or licences are purchased to solve a particular problem or use case. When renewal time comes around, we can evaluate whether that software got sufficient use and warrants a renewal. If not, we may seek an alternative if one exists. However, if the purchase is not recorded, the renewal may take place automatically, but yet the software is never used.

## Maintaining the asset list

So how do we do this? Well we spent a long time thinking about this and we came up with a custom application that had all the bells and whistles. But it was too hard, so we use a spreadsheet. 

You can find the spreadsheet in:

- "Volkis Ops - Documents\Operations\Asset Register.xlsx"

When a new asset is obtained, it must be entered into the register. This is for project management to do, however they need to know the details so the spreadsheet can be updated so please tell them.

To ensure nothing is missed, we will do a quarterly review of the asset register. Project management is responsible for coordinating the review.

## Shared assets

Shared assets may be sent all around the country from consultant to consultant. As such we need to keep track of them in an orderly manner. 

When sending an asset to another staff member, ensure to use registered or express post and take note of the consignment or shipping tracking number. This number should be communicated with the recipient. 

On receiving the asset, that staff member is responsible for ensuring the register is updated by informing project management about the change. 

## Returning of assets

Though unfortunate, it is inevitable that staff will leave Volkis. As part of the off-boarding process, you may be asked to return any Volkis owned assets to the company. This may involve dropping them or shipping them to another staff member. Prior to handing on any laptops of physical storage media hard drives should be completely and securely wiped.

As with shared assets, the staff member who receives the assets is responsible for ensuring project management is aware of the change in location of the asset.

## Secure disposal of assets

If Volkis equipment is disposed of or sold, you must ensure that all data is removed prior to the sale. This could be done through either secure wipe, by replacing storage media and destroying the old storage media, or by removing cryptographic headers from the device if it is fully encrypted.
