---
title: Remote Working Policy
owner: Andy 
last_reviewed_on: 2024-08-06
---

## Overview

Working remotely comes with its own set of perks and challenges. To ensure Volkis maintains a high standard and keeps client data secure, we’ve put together some guidelines for working from home or anywhere, since we don't have an office!

## Data Security

- **No Printing Client Data**: For the security of our clients (and for the environment), please avoid printing any client data. Let’s keep everything digital and secure. If you must print something, say for a client meeting, please shred it as soon as you are done with it! 
   
- **No USB Sticks**: Avoid transferring client data onto USB sticks. It’s easy to misplace them, but, if you need to transfer data via USB please ensure that it is encrypted! Not just a password protected zip either, use something like Bitlocker. Once you are done with the data, format the USB stick.

- **Discretion in Conversations**: If you’re taking work calls, especially ones involving client details, make sure you’re in a private space where conversations can’t be overheard. Avoid having conversations about clients or projects in public places, the walls have ears!

- **Reporting**: Avoid working on client reports in public places, like airports or on the train, people love to read over shoulders. If you are on site, don't work on a report for another client.

## Working Environment

- **Choose a Suitable Location**: When working remotely (or away from your normal workspace), choosing a suitable location will not only help maintain productivity but also ensures data security. Opt for a quiet, private area where sensitive discussions cannot be overheard. Cafes or coworking spaces can be a security risk due to unsecured networks (love some free wifi) and the potential for eavesdropping.

- **Ergonomics Matter**: Make sure your workspace is set up ergonomically. A good chair and desk setup can prevent strain and keep you productive.

- **Ergonomics Tips**: If you are not sure how to set up your desk, check out the [Worksafe Qld website](https://www.worksafe.qld.gov.au/safety-and-prevention/hazards/hazardous-manual-tasks/working-with-computers/setting-up-your-workstation) for some good advice.

## Expenses and Peripherals

- **Expense Peripherals**: If you need any peripherals like a keyboard, mouse, or even a second monitor to make your home office more efficient, you might be able to expense these items, just get approval from Matt or Alexei. We want you to have everything you need to do your best work. If you haven't already, good idea to read the Volkis [Expense page](https://handbook.volkis.com.au/operations/expenses/).

## Staying Connected

- **Regular Check-Ins**: Keep in touch with your team, it can be isolating working remotely all the time. When possible, join the pre-arranged group calls on Slack. If you are feeling lonely, or that you need a chat, drop a message in General and see who is free for a catch-up. 

- **Reach Out for Help**: If you’re facing any issues, whether they’re technical or not, don’t hesitate to reach out. We’re all here to support each other.
