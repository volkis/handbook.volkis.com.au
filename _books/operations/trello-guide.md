---
title: How we use Trello
owner: Victoria
last_reviewed_on: 2024-08-01
---

## What is Trello?

[Trello](https://trello.com) is the kanban board application we use for our project management at Volkis. It is a great way to organise projects visually by breaking them down into managable tasks that can be assigned to specific people. We can also track our progress completing these tasks and check the proximity of our deadlines. Think of Trello as *the source of truth* for our projects, meaning that anyone at Volkis should be able to view the status of a project at any time and have a pretty good idea of where we're at.

This guide will explain the technical aspects of how we use Trello and the bits of automation you need to know about, following a fictional project with a client named **ACME Inc.** (they make dynamite 💥 and anvils). However, the thorough overview of our processes is out of scope for this guide so **before you give this page a read**, go check out [how we manage our projects at Volkis](/operations/managing-projects-at-volkis/).

### The structure of Trello

Trello is made up of one or more **Boards**. A Board is simply a place to put **Cards**. Cards live in **Lists**, inside the Board. Here's a picture to simplify things:

![Engagements board](/assets/img/project-management/engagements-board.png)

This is our **Engagements board** where we track our client projects. The grey rectangles like *Preparation* and *Active* are **Lists** and the white rectangles in them are **Cards**. Each project has its own Card and is generally mapped to exactly **one** proposal. So, 1 proposal = 1 project = 1 Card.

## The anatomy of a Card

You can see our project for ACME Inc. is already in there. When our *business contact*, Wile E. Coyote, accepts our Qwilr proposal, that Card will be automatically created in the **Preparation** list for the project coordinator to initiate the project. Let's take a look at what's inside a Card:

![Project card](/assets/img/project-management/project-card.png)

- The **Project/Card Name** must be consistently used in many places, including in Slack, SharePoint and Resource Guru, otherwise things will break. You'll probably never have to change it manually.
- The **Description** section is where we keep track of background and coordination notes for the project. When any card is created it should automatically have the template to fill out in the description.
- The **Contact details** custom fields are for the name, email and mobile number of our *technical contact*. This is filled out manually by our project coordinator when initiating the project.
- The **Retest expiry date** field will be filled out automatically after a project has been ticked as delivered. The expiry date will be set to 90 days from the delivery date.
- The **Project ID** should already be filled in for you. We go into detail about the Project ID in the next section.
- The **Project Lead** field has a drop down list from where you can select who will lead the project.
- The **Attachments** area will contain at least the links to the Slack channel and the Proposal. You can add more attachments here.
- The **Checklists** area is where you'll see some templated checklists, like *[some] penetration test*. You can add custom ones, too.
- The **Due date** for the project is set on the right side of the card. We set the due date manually to when the draft report is due once we have a confirmed schedule. Remember to **keep this up to date** if things change.
- The **Members** for the project will be the security consultants involved in delivering the work. Adding a user to a Card will allow them to access the project folder and also add them to the Slack channel. You can either click the *Members* button on the right or you can drag the user's icon onto the Card from the Board screen.

### Project ID

A long time ago in a galaxy far far away, you had to fill in the Project ID field manually. This is now done automatically, but it's still important to understand the structure of a Project ID.

We use the notation of **CCC-YYMMDD-I** where:

- **CCC** is the 2-3 character client code, in capital letters.
- **YYMMDD** is the 2 digit year, 2 digit month and 2 digit day of when the proposal was accepted.
- The final **-I** is only used if the same client accepts 2 or more proposals on the same day. The **I** part starts at 0 and is incremented by 1 for every new project.

So for ACME Inc. we might have the Project ID of **AC-240711**. This is the same as writing **AC-240711-0**, but the **-0** can be omitted. If they accepted another proposal on the same day, that new ID would be **AC-240711-1** and if they accepted a 3rd proposal, the 3rd project would have the ID **AC-240711-2**.

⚠ Remember we use the **2 digit year**. Not the 4 digit year. For example, this is wrong: ~~AC-20240711~~

Changing a Project ID is an edge case so most likely you won't ever need to use this information. But if for some reason, you **absolutely** need to change it, you must do this in 6 places:

1. In the Project ID field in Trello;
2. In the Card Name field in Trello;
3. In the corresponding Deal in Pipedrive;
4. In the Project Code field in Resource Guru (only after project creation);
5. In the Slack project channel;
6. In Xero.

### Checklists

Generally speaking, there will be one checklist per project component. You can organise the checklists in whichever order fits the project by clicking on them and dragging. For example, our ACME project has an internal pentest and external pentest component, so we need to reflect that and move the new checklists to the proper location. The Card now looks something like this:

![Organise checklists](/assets/img/project-management/organise-checklists.png)

**Checklist Items** are just as customizable as checklists, so add in all the extra checkboxes that are required to properly follow up the project. In this case, the network segmentation check was a special request by Wile E. Coyote, so we added that extra checkbox to the internal pentest checklist for the consultant doing that part of the project.

![Add an item to a checklist](/assets/img/project-management/add-checklist-item.png)

So who is the consultant doing this part of the work? We need to assign the component to one of our project members. We can also assign a due date for each item in the checklist here:

![Add an item owner and due date](/assets/img/project-management/item-assign-due-date.png)

As we complete tasks for each stage of the project (read each checklist), the checkboxes have to be checked ✔️ as complete by the person who performed them and be deleted if the task is irrelevant to the project. Let's use our ACME project as an example:

- We're doing an internal and an external pentest for ACME. They have asked we deliver a single report containing all the information, so we can go ahead and get rid of the *Report delivered* checkboxes when we don't need more than one report. In fact, we are fine to remove both of them and only leave *Project delivered* as the checkbox the consultant will check as complete because report delivery, in this case, marks the end of this project.
- External pentests normally have a retest component, but we have no findings in the external environment after the work is completed by a consultant. This means there's nothing to remediate, so we can go ahead and get rid of all the retesting related checkboxes.

**Our rule is only check ✔️ an item _after_ it has been completed.** This is good practice, so we all have visibility on the status of the project but also essential for some key automation to work. We'll talk about our Trello automation at the end of this page.

### Due dates for items

Setting due dates for an item on a checklist will help us run smoother projects by telling us what activities should be prioritised. Use the following guide to set due dates for some recurring activities:

- Scheduled jobs; set this to the date when we aim to have or should have a project schedule confirmed.
- Got job requirements: set this separately for each requirement to when we should have the requirement in place.
- Kickoff meeting performed; set this to the date of the kickoff once one is booked.
- Invoice sent; set this to the date when we should send the invoice out only when there's a special invoicing request by the client.
- Debrief complete; set this to the date of the debrief once one is booked.
- Offered retest; set this to the date we're expecting to send out each reminder after one month and then two months.
- Retest performed; set this to match the booked retest date so we remember to send the project to archive once it's done.

Trello wants to be helpful so when we're looking at our board, each card with a due date will show us the date of the next activity we need to complete on each project at the front of the card and some will be marked in colour.

- When a due date is more than a couple days away, it won't have any colour:
![Date on card](/assets/img/project-management/no-light.png)

- When a due date is coming up soon, it will be yellow:
![Yellow date on card](/assets/img/project-management/yellow-light.png)

- When a due date is very soon or overdue it will be red:
![Red date on card](/assets/img/project-management/red-light.png)

When we zoom out to show the whole board, it will look something like this:
![Front of card in board](/assets/img/project-management/front-of-card.png)

## Organising Cards in Lists

So far, we've looked at our [AC-240711] ACME Inc. - Penetration Test 2024 Card in detail...but we will generally have anywhere from 40 to 70 Cards living in our Engagements board at any given time so it's important to consider how to organise those cards to allow visibility across all the projects that need attention.

- In the **Preparation** list, we organise cards from top to bottom by earliest action needed (in any project) so that any tasks that need to happen sooner are visible without scrolling. Once an activity is complete and no longer needs the earliest attention from us, we drag it to its new place in the list.
- In the **Ready to go** list, the order is not important because we have done all the tasks we needed to initiate a project. The last project we have fully prepared will automatically move to the top of this list once all Preparation checkboxes are ticked as complete.
- In the **Active** and **Long-term active** lists, we organise cards from top to bottom by earliest delivery date. This is so any projects that need to be QA'd and delivered sooner are visible without scrolling in both lists. Once the *Project delivered* checkbox is complete, the Card will automatically move to Post-engagement.
- In the **Post-engagement** list, the order will automatically be top to bottom by latest project delivered. Cards here have unfinished business so they should stay in this list until all tasks except retests are completed.
- In the **Awaiting retest** list, we organise cards from top to bottom by earliest retest expiry so that we keep an eye on any retest reminders to send out and move all expired projects to be archived.
- In the **To Archive** list, the order is not important because all the tasks are complete. It's time to say goodbye for good, cards here are just waiting for one of our directors to archive their project folders.

## Notes on automation

We've concluded the basics on how you should use Trello at Volkis! *round of applause* 

🛑 **BUT DON'T STOP READING YET** 🛑 There's one last thing to know. The first order of business for any Card on the preparation checklist always states... *Do project folder, Slack channel, schedule create **(Auto)*** Before you tick this checkbox, make sure that the title of the card includes the project ID.

Notice the **(Auto)** at the end? That tells us that this task is going to be performed automatically for us. Once it's checked, wait a bit and you should see the Slack channel, the project folder and the Project object in Resource Guru be created. The Slack channel will be automatically attached to the Trello card, this allows for our Butler app to tell us when tasks in Trello have been completed in our Slack project channels.

If you want to know even more about the automations we have integrated to our workflow, you can read more about it [here](/operations/automation/workflow-automations).