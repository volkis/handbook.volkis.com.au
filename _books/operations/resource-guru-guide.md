---
title: How we use Resource Guru
owner: Victoria
last_reviewed_on: 2024-08-5
---

## What is Resource Guru?

[Resource Guru](https://app.resourceguruapp.com/hi/volkis/) is the resourcing and scheduling application we use for our project management at Volkis. We use this tool to organise our security consultants' time on billable projects, internal projects, upcoming leave, training time and conferences. We also use it for time management, to track how much time we spend on projects vs. how much time we scoped and make sure we are distributing incoming work appropriately among consultants so that no one is overwhelmed. Therefore, **the schedule in Resource Guru should reflect what has actually happened, what is happening right now and what is most likely to happen in the future.**

In this guide you'll learn about how we use and expect people to use RG at Volkis - maybe you'll even learn some new hacks if you're already familiar with it! Bookings for us can be one of five categories:

- Billable work
- Internal projects
- Study time and training
- Business excellence
- Time off

## Scheduling billable work

The projects we deliver for our clients are considered *billable work*. These can be penetration tests, secure config reviews, red teams...any of the professional services we offer when we're doing them as part of a commercial agreement with a client fall under this category. To book billable work for a security consultant click on the consultant's timeline and the booking window will pop up. If you've followed all the steps in the [managing projects at Volkis](/operations/managing-projects-at-volkis/) guide, the Project object should have been created by now and you can use the search tool to find and select the right Project/Client. Don't forget to toggle the Billable switch on.

When booking a new project, keep the following things in mind:

![Creating a new project booking](/assets/img/resource-guru-guide-project.png)

- Make sure you are booking "duration" and it's 8 hours for a full day task (or 4 for a half day).
- Choose a project to assign this booking to.
- Add what component of the project each consultant is doing in **Details**. For example, "web app pentest".

### Tentative bookings

The first time we are booking a consultant's time on billable work for a project is likely when we're in the preparation stage: when we're trying to find a good time for the project to happen and we haven't presented dates to the client yet. This means the booking is considered **tentative** because if the client doesn't know a project is happening, then it can't happen. Don't forget to toggle the tentative switch on - tentative bookings will have a coloured outline with the client colour and a small calendar icon with a question mark inside.

![Creating a tentative booking](/assets/img/resource-guru-guide-unconfirmed.png)

### Confirmed bookings

Once a client has agreed that their project can happen in the dates we proposed, you can go ahead and toggle the switch to **Confirmed**. This will confirm the time booked for consultants in our schedule and notify them via email (if they have notifications turned on). The confirmed bookings will appear with the client colour fill instead of an outline.

## Scheduling non billable work

Sure, most of us love hacking...but everyone needs a break from time to time or a day to catch up. We take your health, personal and professional development seriously so we encourage everyone to take care of their bodies and minds as well as to dedicate some time to professional training or even an internal project that you're excited about. We refer to everything work related that isn't a client project as *Non-billable work*. You can book yourself or ask to be booked on non-billable hours to block out some time, just let your manager know in advance so we can plan around it.

![Non-billable bookings](/assets/img/resource-guru-guide-nonbillable.png)

### Training

An important part of working at Volkis is to make sure we are regularly putting time towards learning and improving our hacking skills. This doesn't just apply to security consultants and we expect **everyone** - yes, other roles too - to hone their craft whatever that may be. The **Training/Study** category is used when someone is improving their skills. For example: taking a course/exam, reading a whitepaper/blog post, messing around in a lab/CTF, attending presentation days and conferences, doing research or **shadowing someone else on a job**.

### Internal projects

We want to treat internal projects the same we treat our client projects, to improve our own systems and share cool resources with our team. In between client projects or when the work is slow is a good time to refer to our [internal projects board in Trello](https://trello.com/b/4mUpgURm/internal-projects) and we'll find the priority list set by our operations team in the *Ready to start* list in order of importance.

The **Client/Volkis** category is used when we're working on internal projects, but it's important that we **don't create a Project object for it**. We can specify what the project is on the Details section and it should match what our Trello board for internal projects says is happening. For example: Coding a new script, writing a blog post, documenting a new methodology.

### Business Excellence

This category is our founders' joking way of saying they are working on Volkis stuff - it might not be training or an explicit internal project but somehow it's survived about 5 years of updates. It's generally good to use when the founders are doing business trips and visiting clients. **Business excellence** should be used when you're doing Volkis related things, but they absolutely don't fit any of the other categories we've described so far.

### Time off

Step away from work every now and then. Time off is any time you don't want to think about work and just want (or need) to relax/recover. This includes personal leave, sick leave, public holidays, etc. Choose the one that fits best.

![Time off booking](/assets/img/resource-guru-guide-timeoff.png)

## How accurate do we need to be? 🎯

Resource Guru shows a 9am to 5pm work day. Of course, that doesn't mean you have to stick to those specific hours. We are results driven, so it's up to you to decide when to work (taking client expectations for the project into account). Try to keep the booking as accurate to reality as possible without making it a timesheet, your booked blocks should be at least 4 hours, but typically it will be 8 hours for a whole day.

If you're doing overtime, it should be added to the schedule, including after hours and weekends - make retrospective changes when necessary. Keep in mind we are counting on Resource Guru to give us data on time management and tell us if we're not scoping enough time for projects; this is why is needs to be reasonably accurate.