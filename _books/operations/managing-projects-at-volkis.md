---
title: Managing projects at Volkis
owner: Victoria
last_reviewed_on: 2024-08-05
---

## What is this guide about?

Hey you, welcome! This guide is meant as a reference for security consultants and project coordinators alike on how to prepare, control, manage the delivery of and close projects here at Volkis. If that's you and you're new at this, yay! * *Do a little dance* * Congratulations on your new role - it's okay if you're not familiar with our processes yet. Hopefully you'll feel a little less overwhelmed after reading this walkthrough.

## The project lifecycle

The project lifecycle starts when an organisation expresses a need for our professional services and finishes when - commercial agreement in place - we archive a project we've delivered for them. The stages of the project lifecycle are described below, including some general details on roles and resposibilities throughout.

![Project lifecycle](/assets/img/project-management/project-lifecycle.png)

Some definitions to keep in mind when looking at the project lifecycle are:

**Roles** are the functions performed by people at Volkis. **Responsibilities** are the expected engagement and activities for particular roles. **Processes** are the stages a project lives through at Volkis. **Checkpoints** are our event driven meetings. **Outputs** are our stage deliverables.

## Scoping projects

The scoping process generally kicks off when our relationship manager sets up a meeting with a potential client ans gets a scoping consultant involved. Needs from clients might come from a range of places: they may be a regular customer with us or they might have learned about us on a Reddit thread, they may be the customer of a partner or the work might come from an RFQ (request for quote) for govermnment. The RACI (read as Responsible/Accountable/Consulted/Informed) chart below, and at the beginning of each stage from now on, should give you an idea of the responsibilities for each task at Volkis.

![RACI for scoping activities](/assets/img/project-management/scoping-raci.png)

It's hard to define a fixed way of going about scoping work so for the purposes of this guide we will only be concerned with what happens after a proposal is accepted by a client. The written acceptance of a Qwilr proposal is what will trigger our backend project management processes - the tool we use to control projects is Trello, our source of project truth. You can check out the [full guide on how we use Trello here](/operations/trello-guide/).

When you open a card in Trello, the project management activities are actionable items in checklists that go through the entire lifecycle of a Volkis project from initiation to archive. Generally, the templated checklists will include all the tasks that need to happen for a project to exist, be scheduled and be manageable - with some exceptions - so we'll use the items in these checklists to define the processes for preparing and following up a project through to archive.

## Initiating projects

The preparation checklist represents the time from a proposal being accepted to a kickoff meeting for the project taking place with all the stakeholders involved (the delivery team, the client, the account managers if applicable and anyone that needs to know about the project including why, when and how it will happen).

![RACI for preparation activities](/assets/img/project-management/preparing-raci.png)

### Background notes

Starting off a project with a bit of background from the presales team is the first step in setting up a project for success. We do this so that we all have a quick overview of what we know so far and to answer key questions such as "what makes this project valuable to our customer" and "is there any hard deadlines that we need to accommodate" early on to plan accordingly. This is the first piece of information we will get to understand the nature of a project and its purpose within an organisation. Generally speaking, the relevant information comes from our relationship manager and scoping consultants, but when scoping hasn't been performed by consultants at Volkis (in the case of work through partners or won RFQs) fill in the description template with all available information at the time and continue to update throughout the project as needed. This is highly customizable, we're looking for a good balance between fact and nuance, so only include the relevant fields and feel free to remove the ones that don't apply.

![Background notes template](/assets/img/project-management/background-notes.png)

### Ticking *Do project folder, Slack channel, schedule create (Auto)*

This is the easy part and **has to be done first** for the project to exist within the tools we use. Notice the (Auto) at the end? That tells us that this task is going to be performed automatically for us. The checkbox exists merely as a trigger for this task. Once it’s checked, wait a bit and you should see the Slack channel, the project folder and the Project object in Resource Guru be created. The Slack channel will also automatically be attached to the Trello card and the project coordinator and relationship manager should automatically be added to all Slack channels - if this doesn't happen have Alexei, who does all our automation, look at it.

### Adding job lists

When the card for a new project is created, it doesn’t have any tasks for the actual project work yet. We need to add these, and we can do that by pressing the *"Insert pentest checklist"* button on the right.

![Insert pentest checklist button](/assets/img/project-management/insert-checklist.png)

This will add a generic checklist to the card called \[some\] penetration test, but it's customizable to whatever we need it to be and should be changed to suit the project.

![Some penetration test checklist](/assets/img/project-management/some-penetration-test.png)

However before we do all of that, what is this project even about? Is it a penetration test, a red team, or is it an essential 8 assesment maybe? Open the proposal under Attachments to find out, this is the second piece of information we get to help us understand the project.

Once we've read the proposal and know what kind of work needs to be done, we can change this entire checklist however we like to fit the project. The nature of Volkis offering bestpoke services means that projects will not always have the same requirements from our security consultants. Insert as many checklists as the project needs. Usually, as it's explained in the [Trello Guide](/operations/trello-guide/) this would be one checklist per project component. For example, one checklist for the mobile app pentest and one checklist for the internal pentest as these may be done by different consultants at different times and need separate reports to be delivered (or not). But what if the proposal leaves you with more questions than answers? What if you don't understand the context, the wording or what the requirements entail? You can ask the relationship manager and the consultant that scoped the work if you have any questions but don't worry too much for now, we'll get to that when we do the project handover.

### Identifying stakeholders

Who are the stakeholders of this project? There's a few contacts here that we should include:
- the business contact is the person who has often procured and approved a project, and likely the person who will be the receiver of the final report but don't assume this is always the case.
- the account manager is the person who would have procured the work if we're working through a partner, and they might be the receiver of the final report instead of the end client so make sure to check this information.
- the technical contact is the person who our security consultant will be referring to with technical questions and access needs (for smaller projects the business and technical contact often can be the same person).
- the support contact is the person who will be doing help desk activities for our security consultants in the case that the technical contact appoints them as such, but this is only ever relevant for complex access requirement projects and when there is third party systems in scope.

As a general rule, **the client custom fields are reserved for the technical contact** so that security consultants have a direct line for critical issues to be raised. There's a few places that information can come from; we can see in Pipedrive who did the presales and who the (partner) account manager is for the project and go ask that person for the client contact details if needed. Add the contact details to the custom fields at the top of the card: Name, Email and Mobile and keep the contact list in the card description up to date with the relevant people as the project progresses.

![Client contact details fields](/assets/img/project-management/contact-details-fields.png)

Sometimes clients won't have a mobile listed by default here but it's important we have this to ease communication. We must fill this in so that everyone doing work on this project knows who to contact in case of questions or for updates.

⚠️ **The contact we fill in here might not necessarily be the person we send the report to once the work is done. When dealing with clients through partners, partners might want to QA the report before it's sent off to their customer or deliver it themselves without going through Volk Messenger (our own secure file sharing system).**

### Resourcing

By this point, you should have an idea of who will be a good fit for the project. We need to add all the consultants that will be working in delivery to the card and assign a lead to the project. The lead will be responsible for the general overseeing of the project, gathering everyone's work and delivering the report to the right people.

### Handover

A project handover happens when the scoping consultant and the relationship manager pass on their intimate knowledge of a project purpose and scope gathered during the scoping cycle to the project lead aiming to ensure continuity in the delivery. For simpler projects, the background notes left in the card description and particularly the motivation statement might be enough to consider a project handed over. Sometimes, and most definitely for complex projects, a short huddle in the dedicated project channel will be ideal to make sure we're all clear on what needs to be done. The main purpose of a handover is that everyone's expectations - scoping team, delivery team and client - align.

These are some questions to help guide a handover:

- Do we know what the client expects/needs from this project?
- Are there any unknowns we should be looking out for?
- Were there any contraints considered when scoping?
- Are there any potential challenges to us getting what we need to deliver the project?
- Is there any particular structure to the time that was scoped? Why?
- Are there any time constraints that we need to accommodate?
- Does the project lead now have a good understanding of the scope? 
- Does the project lead know what to expect from the client at kickoff?
- Does the project lead know what they need from the client at kickoff?

If you completed all the tasks prior to this point, all the consultants doing the work should have been added to this Slack channel automatically. By the end of a handover, the consultant/s doing the work should have a better understanding of what they are doing, who for and why they are doing it. The consultant/s should feel comfortable with the time scoped for the project or be aware of time limitations if there are any. Having a better understanding of the project should allow us to schedule the work appropiately, according to the needs of the client and the specific project requirements if the requirements in the proposal or the statement of work are vague.

### Scheduling

Scheduling is done in Resource Guru - [check out the full guide on how we use Resource Guru.](/operations/resource-guru-guide/) You can plan things out tentatively here but before you confirm the schedule for a project, we need to speak to the client to get confirmation that they will be ready to get the work done when we are expecting to book them. We've already spoken to presales and the project lead so **let's make sure we are all in the same page, including the client**.

Give the client a call (this is why we need a mobile number for them) to find out if our expected timeline matches the clients' and if there are any requirements we are not aware of yet (this could be anything from internal onboarding processes to their key people out of office for a certain period of time). In a best case scenario they would have mentioned any deadlines or extra requirements they have to the consultant scoping the work but that might not always happen. Since we're already speaking to the client, this is also a good time to ask when they are free for a kickoff - more on this later when we talk about kickoffs but for now, please follow the naming convention for all kickoff meetings: **"Kickoff: \[Project name\]"**.

Follow up this call with an email going over everything you discussed in writing, so that dates, times and any action they need to take towards organising requirements for us solidify in their minds. 

Once you get confirmation from the client, you can create or confirm their tentative booking in Resource Guru and send all the parties involved an invite to kickoff. The project object in Resource Guru should already be created since it's the first task we completed, remember? Simply make a new booking against the consultant/s involved and assign the right project to that booking. If there are multiple parts to the project, add what part the consultant/s should be doing in the Details section.

### Project deadlines

Once the booking for a project is confirmed by the client, set the due date on the card to the last day of technical work scheduled, which is the day the project lead should have a draft report ready to be QA'd by another consultant in the team. We do our best to set appropriate expectations by allowing up to a week after all the technical work is complete to deliver a report, so consultants have some wiggle room to make changes after a draft report goes through QA, which can take a couple days. **Remember to keep this up to date if things change as the project unfolds.**

### Welcome packs

When working with direct clients, we have welcome packs prepared that will give them general information on the service/s they booked and what to expect from the engagement before, during and after it happens. Include the link to the relevant welcome pack in the follow up email you send the client after you just called them to book the work and organised a kickoff meeting. When working with partners, they might not want us to act as a separate entity so they unfortunately miss out on things like our welcome packs.

- [Penetration testing welcome pack](/services/welcome-packs/penetration-testing-welcome-pack/)
- [Phishing campaign welcome pack](/services/welcome-packs/phishing-welcome-pack/)

### Gathering requirements

Well, what do you mean job requirements? That's quite vague. This list item is there to be customized because the requirements will change from project to project although some might be common to most projects, for example getting confirmation for the in-scope URLs. You can find the project requirements in the "What we need" section of a proposal. However, anything that the proposal is missing but is still required should also be included.

For better visibility of our progress, we can edit this single task into multiple tasks to accurately reflect what needs to happen and when (you can add a deadline on the side for each task). It's a good idea to be as specific as possible, especially if the requirements that need actioning have dependancies and/or require extra time to be organised, but that really depends on the project. This could include shipping over physical materials (consider how long that will take), going through long onboarding processes (some projects require this for everyone who will access certain data), credentials for auth testing (account creation sometimes needs multiple people involved before it's approved) or hopefully something simple like sending documentation to us.

Regardless of what they are, they should all be in place ideally one week before the project is active, but as a general rule they must be *tested by kickoff* which if all goes well, will happen the week before the project is due to start. This way, if anything goes wrong we are allowing for a whole week to fix any access/shipping issues. When requirements are left to be done passively by the client, there is a higher risk of the project not starting on time. We don't want to cause delays for ourselves, our partners, or our clients so we are accountable for what we need from clients. We should aim for early gathering of requirements and for consultants to test their access as soon as they are provided.

When communicating what we need to clients, short and clear works best:

- We provide a list of reqs and explain them further if there's questions
- We test access a week before the project is due to start
- We will delay the project if the access condition is not met
- If the access condition is not met, we may provide some time to fix it at our discretion

The ideal lead time for project preparation is one to two weeks. However, some projects might have a shorter lead time and that also presents a higher risk of the project not starting on time. We need to be clear with clients that if the access condition is not met by kickoff when there's shorter lead times, then their own project time will potentially be sacrificed if we have to wait for access. Our clients need to be aware and agree to this condition.

### Kickoff meetings

While this should be a scheduled meeting already, it's a good opportunity to revisit the naming convention: **"Kickoff: \[Project name\]"**. This is so Outlook quickly shows us what meetings are a kickoff and to what project they refer to in our calendars. There's no need to include the project code here, we only use the project code for internal reference.

Remember you already called and scheduled this meeting? Kickoffs generally need to happen a week before the project is due to start as a last check that we are all in the same page. It's usually the first time the consultant/s performing the work are having a screen-to-screen conversation with the end-client so don't forget to make introductions and make sure everyone that needs to be involved is present.

The consultant/s will use this time to confirm that they have everything they need (the requirements that we have diligently procured for them beforehand), sanity check what we'll be doing, get any information we are missing and answer any questions for the client's team.

Once the kickoff meeting has taken place, you can go ahead and tick that box. Now that the entire Preparations checklist is complete, the card will automatically be moved to *Ready to go*. This is where we aim to be with all projects a week before they are scheduled to start. Do another little dance, that's the sound of a project on track to success!

### Project risks

The consultant must ensure that we identify and control any major risks that pop up in our projects. This could include:

- Risks that might cause the schedule to slip.
- Risks that we might not be able to complete the scope of works.
- Risks to the information security of the client, for instance when opening holes in their firewall or when deploying malware.

Any major risks should be documented, with appropriate risk mitigations agreed upon with the client.

## Controlling active projects

Projects become active once a consultant has begun the technical work period allocated as scheduled in our [Resource Guru](/operations/resource-guru-guide/) calendar. **It's important that the project card is manually moved by the consultant who starts the technical work** from the *Ready to Go* list to the *Active*/*Long-term active* list **in order of earliest due date at the top to latest due date at the bottom**. We will expand on why we do this in a minute, but keep in mind the project coordinator and other consultants should have visibility on the state of the project as it progresses.

### Active project or long-term active project?

At this point you might be wondering why we treat some projects differerent to just *Active* and they are instead *Long-term active*. What is the difference and why does their expected length matter to us? Most of the projects that come in at the moment of writing this guide belong in the *Active* list.

#### (Short-term) active projects

This is our "default" project type, as is the case for most types of penetration tests and secure configuration reviews - which are our "bread and butter" as Matt puts it. These have reasonably finite, predictable scopes that can be thought of as our "standard" way of working: we get a project, we hack things, we put all our findings in a report and we deliver that report to the client - work done and invoiced!

#### Long-term active projects

The nature of long-term projects is that they will take *...drumroll...* longer to complete, often because they are significantly more goal-driven than they are time-driven, or they specifically need to be done in spaced out steps for reasons that will vary depending on the client, the project and its ultimate purpose - this will be the case with red teams, GRC and advisory projects.

For long-term projects, there should be regular check ins with the consultant and a communication plan with the client depending on the nature of the work and the preference of consultants and/or clients to discuss how the project is progressing and if the goals need to be revised. Long-term projects require a lot more planning and monitoring because they are more than likely to evolve over time so make sure to keep a consistent line of communications and set up a system for invoicing the work while in the preparation stage to make life easier for our accounts team (that's Nat).

### Why organise projects by delivery date?

Monitoring active projects is all about visibility. You can't see what is not there and it's hard to follow along if the source of truth (which is Trello) can't be trusted to be accurate. While it may not seem all that important to keep this organised, we have to make sure we focus on the projects that need to be delivered sooner, so no deadlines are missed or forgotten. It also helps everyone at Volkis stress less when things are not done last minute - QAs can take a long time when everyone is busy hacking!

### What to look out for?

Check in with consultants often and keep up to date with the dedicated project channel to know what is going on during the technical work period. Provide support when needed by facilitating communication between teams and expect the unexpected to happen! Sometimes things don't go according to plan - it might not be important to find the underlying cause immediately (that's what post-mortems are for) but rather to find a solution. The solution sometimes requires a change of contract or a change in schedule so there's a chance other projects will be affected. It's important to let all affected projects know when there will be changes to their schedule to give them time to plan (they might have dependancies of their own that we don't know of).

- Do we need to adhere to a testing window to not disturb business operations?
- Are we following the communication plan we agreed on?
- Are clients responsive when we need clarification?
- Do we need any walkthoughs or meetings for things to progress?
- How are consultants doing in work and in life?
- Do security consultants need any support?
- Is the project going according to plan?

## Managing project delivery 

Security consultants should be keeping the \[some\] penetration test checklist updated according to their progress - so if you see anything that doesn't look right just ask the project lead.

![RACI for delivery activities](/assets/img/project-management/delivery-raci.png)

**The time allocated in the schedule should generally be enough for all the technical work to be completed from start to finish, including active testing time, the compiling of a draft report, QAing of a report from another project, having a debrief for a past project and having a kickoff for an upcoming project.** The due date for a draft report is the last day of technical work scheduled on the calendar.

If we are not progressing as expected and predict it will be overly challenging to get the draft report in when it's due, the project might be underscoped, or there might have been extra scope creeping in last minute - **be on the lookout for scope creep throughout the project**. Sometimes, added scope will require a change of contract for no crimes to be commited and we like to stay away from crime as much as possible. Whenever the scope doesn't match the expectations it should be noted in our documentation for the project and the feedback should make its way to the person who did the scoping during the project closing cycle.

### Quality management approach

All our quality control and quality assurance is done internally by security consultants who are not part of the project. Each security consultant is given the tools to manage their QA requests and deal with incoming ones. Once the consultant has a draft report ready for review, they will put a QA request and have another consultant assigned appropriately by our internal tool MayGibbsQA.

Once the peer review is complete, the security consultant will need some time to make any changes that need to be made and once that's done, it's time to release the report to the client.

### Project delivery

Well done everyone! We've made it this far in the project lifecycle so it might seem like we can put this project behind us but that's not quite true (yet). Once all the components (reports, workshops, etc.) of the project has been completed and swiftly delivered to the right people, we need to make sure that this item gets ticked for a bit of automation to happen - thanks Alexei. This should trigger the project card to move to the *Post-engagement* list, a retesting expiry date to be assigned and most important, a "to be invoiced" tag for accounts to be attached so we can invoice the client for a job well done.

## Closing projects

The *Post-engagement* and *Awaiting retest* checklists represent the time from project delivery to the archiving and encrypting of the project folder. The closing cycle for our projects is just as important as the preparation cycle as its designed to regularly review our performance in time, scope and quality management - by focusing on the feedback we get both internally (from consultants) and externally (from clients) we aim to achieve maximum hacking greatness in future projects and continue to improve our services.

![RACI for closing activities](/assets/img/project-management/closing-raci.png)

### Post-project housekeeping

Updating templates in RR relates to technical project activities, so this has to be done by security consultants, ideally as soon as they finish a project including removing any files that were used as part of the project which are no longer relevant.

### Invoicing

Once a project card has been tagged as "to be invoiced", accounts will action this task. Most projects won't need a lot of support from project coordinators at this point, unless there's a change of contract to be aware of, a PO needs to be associated for finance teams to process our invoice, or there's milestone payments to be calculated for long-term projects. In the case that there is invoicing changes to raise, message Nat directly on Slack. Accounts are not involved in the day to day project activities so assume they have no information other than the signed proposal and provide context.

### Organising a debrief

Debrief meetings are a time for the client to get a walkthrough of the report and any additional clarification that might be required from the consultant/s that performed the work for them. Similar to kickoffs, debrief meetings follow the naming convention: **“Debrief: \[Project name\]“**. Again, this is so Outlook quickly shows us what meetings are a debrief at a glance in our calendars.

Ideally, consultants would have offered a debrief to the client when they delivered the report and the project coordinator will have scheduled them accordingly - this information would generally be found in an email chain. Sometimes, it can take a while until we hear back from clients so it’s good to keep track of communications and once a debrief is in place, label the project card with “debrief pending”.

**Once the debrief meeting is done, the security consultant should tick this box as complete.** This will remove the "debrief pending" tag from the card automatically. Great! We're getting to the end I promise. The project card needs to be manually moved to the *Awaiting retest* list when a retest may apply or *To archive* when a retest is not part of the service.

### Customer feedback

It's good practice (and a somewhat informal process for now) to get feedback from clients on the quality of our work, our team, our processes and our communications while it's still fresh in their minds. So while our relationship manager, our consultants or our project coordinator is sitting on the debrief with them, find a good time to ask clients if they would like to give us any feedback:

- How was the report? Was it easy to understand and was it actionable?
- Did the client get what they wanted out of the project?

Let them call out the good, the bad and give us their perspective. If we ask the right questions, this exercise should give us more clarity on what we should keep doing, what to change and how to continue improving our professional services. 

### Internal feedback

It's important we get feedback not just from clients, but also from the consultants that take on the work someone else has scoped for them. Scoping is not a perfect science and there's bound to be room for improvement in most projects as well as uncertainty when it comes to more complex projects. When it's simple enough, all we need from security consultants is a few sentences as a comment on the project card or the Slack channel going over their scoping notes, the good, the bad and anything out of the ordinary that they think relevant for the presales team or for the project coordinator to know about and consider in the future. When a few sentences are not enough and we need to have a proper conversation with the team, we do a post- mortem.

### Facilitating a post-mortem

A post-mortem is not part of the normal project cycle but rather a "sometimes meeting" with the team to discuss what went wrong if anything did, what could have gone better and actions we can take to improve the way we do things and avoid problems. Anyone in the team can tag a project with "post mortem needed" when they feel like there's something to be gained from having an internal debrief post-project that goes beyond the normal amount of feedback they would usually provide. This is a particularly good practice to go through lessons identified in projects we do on an irregular basis to brainstorm better processes and approaches.

### Retests

Last but not least, our complimentary retests (**for external and web application penetration tests only**) are our way of motivating teams to fix any internet exposed vulnerabilities we found in their environments. The goal of this service is to ensure that the remediation efforts post-project have been successful and the risk has been mitigated. We have prepared a welcome pack for clients answering some common questions about how it all works:

- [Remediation test welcome pack](/services/welcome-packs/remediation-testing-welcome-pack/)

Either our relationship manager or the project lead will check in with the client a month after and then two months after we deliver a project to see if they would like to make use of this service. In the case that they don't, that's okay and we can get rid of the checklist item. In the case that they would like to retest their fixes, then we'll have to find a time (one day is more that enough) for the same consultant that performed the original work to do the retest for continuity.

### Reporting project success

We've reached the end of the project! The purpose of closing out activities is figuring out whether the projects we work so hard to deliver actually go to plan or not. This is this project coordinator's personal favourite because it tells us a lot in a way that's easy to understand and compare. We gather all the information from proposals, Resource Guru, feedback left by consultants, clients and ourselves and fill out our project success reports that live in VolkisOps - Documents/Operations/Project Management.

This concludes the basics to preparing, controlling, managing delivery and closing projects here at Volkis...for now :D This page is getting updated often as things tend to change quickly for us. It's a lot of information, so thank you for reading - I wish you all the success in your project coordination endevours!
