---
title: Expenses
owner: Alexei
last_reviewed_on: 2024-05-20
---

## Overview

As part of your job you're likely to incur costs on behalf of Volkis. We try and make it easy to get those costs reimbursed. Expenses are paid to the same bank account as your salary.

## Spending Volkis money

It is perfectly okay for employees to spend company money when it is related to their duties. As a general rule, treat Volkis money like it was your own. That means, spend what is needed, but avoid overindulging. You don't have to stay at the cheapest hotels, but the Ritz-Carlton is probably a bit much. If in doubt, ask your manager. 🙂

## Submitting an expense

We use **Xero Me** for submitting, filing, approving, and paying expenses. With this app you can also view pay slips and make leave requests. It's available here:

- Android: <https://play.google.com/store/apps/details?id=com.xero.payday>
- IOS: <https://apps.apple.com/us/app/xero-me/id991901494>

When you started employment you should have been invited to your personal Xero account. This is used for payroll and expenses, and is the account you should use to login to Xero Me as well.

The instructions for submitting an expense can be found here: <https://central.xero.com/s/article/Create-a-new-expense>

## Project-related expenses

Ask yourself, _"Am I making this purchase solely because I am on this project?"_ If the answer is **"yes"**, then it is a project-related expense.

For project-related expenses, you must add the project ID to the end of the description, in brackets. E.g. "Domain for phishing **(AC-230224)**".

![Assigning a project based expense using the project code](/assets/img/expenses/project-code.png)

You must also select the **Direct Client** in the _Assign to customer_ field. If the direct client is the partner then you should enter the partner into the field. The direct client is generally referred to in the project code (for instance `AC` refers to **ACME Industries**).

![Selecting the direct client](/assets/img/expenses/choosing-customer.png)

Still use the correct category (examples below) whether the expense is project-related or not.

## Volkis expenses

There are specific _Assign to customer_ fields that are used for internal Volkis expenses:

- **Volkis Marketing**: Sponsorships, social media and marketing consulting, marketing collateral, as well as flights, travel, accommodation, food, per diem expenses related to travelling to marketing events.
- **Volkis Presentation Days**: Flights, travel, accommodation, food, and per diem expenses related to presentation days and hacker houses.
- **Volkis Training**: Any expenses relating to training including conference and certification fees, training course fees, flights and accommodation for training, food and per diem while attending training, additional software or hardware bought specifically for training purposes.

If you've got expenses that fit those categories then use the _Assign to customer_ field to select one of them:

![Selecting the direct client](/assets/img/expenses/volkis-training.png)

If there are expenses for Volkis that are not in those categories, assign the customer as "Volkis".

## Expense categories

This section contains a list of categories and when to use them. It's important to get the right categories - the categories can have tax or accounting implications. If you're in doubt about what category to use, ask your manager.

### Staff Amenities

According to the ATO, the provision of things such as morning tea to employees (and associates of employees) during the working day is not classed as entertainment but as a staff amenity and in most cases is deductible. Morning and afternoon tea includes light refreshments such as tea, coffee, fruit drinks, cakes, biscuits. It does not include alcohol or food for entertainment purposes.

- Morning/afternoon tea for employees
- Coffees and snacks
- Not appropriate for:
  - Lunches and dinners (should be "Entertainment" or "Travel")
  - Food for non-employees (should be "Client Meetings and Events" or "Entertainment")
  - Training food (should be "Travel")

### Client Meetings and Events

- Food, coffees for meetings and events with clients
- Additional costs such as booking fees

### Marketing and Advertising

- Advertising fees including online advertising
- Sponsorships for conferences
- Graphics design and consultants for marketing

### Bank Fees

- Annual fees for bank accounts and credit cards
- Payment transaction fees

### Consulting & Accounting

- Accounting expenses
- Xero fees
- Third party contractors

### Entertainment

- Lunches, dinners, alcohol expenditure
- Costs for internal non-training events (including dinners after presentation days)
- Not appropriate for:
  - Food during training, including while travelling to conferences and presentation day (should be "Travel")
  - Food during travel for client engagements (should be "Travel")

### Freight & Courier

- Costs for sending packages between Volkis employees
- Delivery costs for laptops, hardware, training materials (can also be covered under "General Expenses")
- Sending packages to clients such as the delivery of a jump box

### General Expenses

- Anything not attributable to other categories
- Not appropriate for:
  - IT infrastructure, hardware, laptop adapter costs (should be "Information Technology Expense")
  - Food, travel, and accommodation to client sites (should be "Travel")
  - Alcohol purchases (should be "Entertainment")

### Insurance

- Professional indemnity, cyber risk, WH&S insurances

### Legal expenses

- Lawyer fees

### Light, Power, Heating

- Utilities for home office
- Currently only to be used by Matt & Alexei

### Information Technology Expense

- Fees for Pipedrive, Xero, Qwilr, AWS, infrastructure costs
- Hardware expenses for laptops and workstations
- Costs for servicing laptops, IT infrastructure
- Fees for dehashed.com, VMware licenses etc.

### Office Expenses

- Internet, phone, and utilities for home office
- Currently only to be used by Matt & Alexei

### Printing & Stationary

- Printing of business cards

### Rent

- Rent for home office
- Currently only to be used by Matt & Alexei

### Training

- Fees for conferences and training courses
- Online training costs
- Software and hardware costs for the purpose of learning
- Professional certifications
- Ongoing membership fees for societies such as AISA, ISACA, ISC2, etc.
- Not appropriate for:
  - Travel costs, food or accommodation spent whilst at a conference (should be "Travel")

### Telephone & Internet

- Expensible phone plan costs
- Expensible internet costs

### Travel – National/International

You can claim transport and travel expenses you incur when you travel while performing your work duties.

- National/International travel for meeting partners, clients, personnel, etc.
- Trips between workplaces
- Ride-share or ride-sourcing (i.e. Uber, etc)
- Flights
- Train, taxi, boat, bus or other vehicle
- Accommodation
- Hire car
- Fuel for a hire car
- Meals (food and drink) while travelling - only when travelling outside of your city
- Expenses which are incidental to your overnight travel
- Expenses you incur for quarantine and testing when you travel for work purposes
- Tolls
- Not appropriate for:
  - Meals within your local city (should be "Entertainment" or "Client Meetings & Events")
  - Fuel for your own car (not expensible)
  - Mileage (claim as a tax deduction)

Select **Travel – National** for all travel costs as listed above when the costs are incurred within Australia

Select **Travel – International** for all travel costs as listed above when the costs are incurred overseas.

### Car related expenses & mileage

We do not pay expenses for the use of your own car for business purposes. You may be entitled to claim car related expenses on your yearly tax return.

There are two methods to calculate your entitlements:

1. [Cents per kilometre method](https://www.ato.gov.au/Individuals/Income-and-deductions/Deductions-you-can-claim/Transport-and-travel-expenses/Motor-vehicle-and-car-expenses/#Centsperkilometremethod)
2. [Logbook method](https://www.ato.gov.au/Individuals/Income-and-deductions/Deductions-you-can-claim/Transport-and-travel-expenses/Motor-vehicle-and-car-expenses/#Logbookmethod)
