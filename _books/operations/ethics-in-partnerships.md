---
title: Ethical guidelines for Volkis partnerships
---

![Billy Madison Business Ethics](/assets/img/billy-madison-business-ethics.gif)

This page is intended to provide the reader (that's you!) with information on how we, as a company, plan to act in sticky situations. [Transparency is one of our core values](/company/volkis-values/#transparency), so we want to make sure that everyone who works with us knows what they are getting. As you can see from this Handbook, we take this seriously.

Volkis provides services both directly to businesses and through contracted work commissioned by a connected business; both are considered clients. We'd like to think that we are our clients' ally, providing the results they need to incite change and improvement in their security.

We value our relationships with our clients and partners. Good communication, integrity of our work and reputation is paramount to maintaining long-standing, valued partnerships that are mutually beneficial. We understand that partnerships work both ways and as such, we accept that a situation may be reversed. These guidelines reflect not only how we will respond to an ethical grey-area but how we would expect our partners to act in the same situation.

This document will grow over time as we come across new scenarios and complications as part of running a business. Please feel free to reach out to Matt or Alexei if you have any concerns about these guidelines.

## Definitions

**End-client:** The organisation on which we are performing a penetration test or other work. Their infrastructure is what is in scope.

**Partner:** An organisation working with Volkis, who might ask us to work on their behalf for their clients. Or, the reverse.

**Active End-client:** An End-client who we've performed work for in past 12 months, or provided a proposal to a Partner to do work on within the past 3 months.

**Known End-client:** A End-client of a project in which Volkis has been contracted by a Partner to complete some or all tasks. Known End-clients may or may not be Active End-clients.

## Guidelines

The following is a list of ethical guidelines the Volkis team follow to establish, build and maintain healthy, valuable relationships with our partners.

- All Volkis proposals and agreements are strictly confidential and will not be acknowledged or discussed with any third party. This includes but is not limited to:

  - Project proposals
  - Existing client relationships
  - Pricing
  - Agreements
  - Employment applications or opportunities

- Volkis values our existing relationships with our long term partners above winning work when considering a new client. If a new client or project is likely to cause any conflict or distrust with an existing relationship, we may postpone or reject the request for proposal.

- Volkis team members represent both Volkis and the partner organisations for which they are contracted. As such, any opportunities presented while representing a partner will be referred back to the appropriate account manager.

- Volkis considers our Partner, who has requested a proposal, as the owner and contact for all works in the project. Volkis will communicate directly with the End-client unless explicitly told not to by the project owner. Communications in regards to changes to project scope, or to raise any concerns by any parties involved in the project (such as the End-client or a partner of the project owner) will be directed to their respective account manager.

- If asked about our employment with a Partner, we will inform the End-client that we are contractors for our Partner. Any concerns will be directed to their account manager.

- Volkis will never knowingly approach a Partner's **Active End-client** about working directly.

- Volkis will not engage in new projects proposed by an **Active End-client** without written approval. Gaining this approval is up the Active End-client and we will verify the approval with our Partner.

- Volkis does not know all client relationships of our Partners. Due care is taken during partner onboarding to prevent conflicts of interest; however, some may occur post onboarding and will be handled on a case-by-case basis.

- Above all else, Volkis will do everything possible to maintain a healthy working relationship with our Partners and clients.

## Example Scenarios

To ensure full transparency and trust, we have taken the time to consider the ethics and consequences of some situations that may arise, to give our partners a clear understanding of how we intend to operate.

### Scenario 1 - An Active End-client of a Partner has approached us to work directly with them on an upcoming project.

End-clients will be advised that as per our partnership agreements, all proposals and transactions should take place through their respective partner.

An exception can be made if the End-client has obtained written approval from the partner to transact with Volkis directly.

**Example:**

Volkis has recently (within the last 12 months) performed a penetration test on Client A's environment through Partner A. Client A also requires an audit of existing security controls. As Partner A is Client A's Managed Service Provider (MSP), Partner A does not want to be involved. Approval is granted for Client A to work directly with Volkis to ensure the independence of the work.

### Scenario 2 - A Known End-client of a partner has approached us to go direct after 1 year since last interaction.

As many businesses require tests completed annually as part of their compliance or internal security practices, Volkis will refer the End-client back to the partner for subsequent testing.

However, should a period of 12 months pass since a project in which Volkis was contracted, and no proposals have been requested by the respective Partner for the Known End-client within the last 3 months, Volkis may interact directly with the End-client.

**Example 1:**

Client A contacts Volkis in December 2021 directly, stating that Volkis (through their Partner) completed their previous annual penetration test in January 2021 year, and that they would like a new one completed in January 2022.

As the previous project was completed within 12 months, Volkis would refer Client A back to Partner A to request a proposal.

**Example 2:**

Client A contacts Volkis directly, stating that a web application pentest was completed in March 2020 by Volkis (through their Partner). Client A contacts Volkis in June 2021 and needs a new web application to be tested and would like to work directly with Volkis to do so.

Since more than 12 months have passed since the last project for Client A, Volkis may choose to work directly with the End-client.

### Scenario 3 - Volkis representatives meet a partner's client at the partner's event. The client asks us if they can engage Volkis directly.

During an event hosted by a partner, Volkis team members consider themselves representatives both of Volkis and the partner. As such, any opportunities presented during the event will be referred back to the partner.

### Scenario 4 - Volkis is contracted to perform tests on an End-client through a Partner's partner. The Partner's partner asks if we would be willing to engage directly with them instead on the current project.

Volkis will honour any existing proposals with the partner who has requested them. As such, the request to work directly with Volkis on an existing project will be denied, and Volkis' Partner's client (the Partner's partner) will be referred back to their account manager.

**Example:**

Partner A has contracted Volkis to perform a penetration test on an client's environment. The project has been commissioned through Partner A's partner (Partner B), who is the MSP for the client.

Partner B reaches out to Volkis to perform the task directly. Volkis will deny the request as we are contracted through Partner A to complete this task.

### Scenario 5 - A Partner's partner asks if we would be willing to work directly with them for a different project or client.

Similar to the scenarios in which the client contacts Volkis to work directly, a partner (Partner B) of a partner (Partner A) would be required to gain approval from Partner A in order to engage in works directly with Volkis.

However, if more than 12 months have passed since Volkis' last project with Partner B, and no proposals have been submitted by Partner A for projects involving Partner B in the past 3 months, Volkis may conduct business with Partner B directly.

### Scenario 6 - Two Partners ask us to work with the same End-client.

All Volkis proposals are provided confidentially to our Partners.

While it is possible that two Partners will request proposals for the same End-client, often the scope of work differs between the Partners with different interpretations of the End-client's request, reporting requirements or support.

In this scenario, Volkis will submit a proposal to each Partner based on their individual, specific requirements.

### Scenario 7 - An employee of one of our Partners asks us for a job.

Volkis will not exclude current or past employees of clients or Partners from employment candidacy. All employment opportunities, applications and offers are handled with strict confidentially.

In the event that a new hire comes from one of Volkis' clients or Partners, it would be expected that the new employee would communicate openly with their current employer about their decision and destination in order to prevent any conflicts of interest.

### Scenario 8 - A Known End-client puts a job up for public tender. Volkis are invited to present a proposal.

Volkis may submit a proposal based on our interpretation of the End-client's requirements.

Similar to Scenario 6, in which two Partners request a proposal for the same End-client, should a Partner request a proposal that they will submit for consideration themselves, Volkis will ensure that all proposals created will be confidential and meet the Partner's interpretation and requirements without bias.

## Last words

As you can see, certain scenarios are ethically difficult to navigate when you take into account relationships, trust, the relatively small size of the infosec industry and a need for confidentiality. Although our Partners should expect us to act in the above manner, we may choose to act in a way that is more favourable to our Partners than outlined in these scenarios.
