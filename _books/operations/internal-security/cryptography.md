---
title: Cryptography policy
owner: Matthew Strahan
last_reviewed_on: 2024-09-23
---

Encryption is used within Volkis to protect sensitive data at rest and in transit. The following encryption mechanisms are in use:

- TLS encryption protecting our web interfaces.
- Encrypting data in transit
- Encrypting data at rest.

This page shows how encryption should be used in these circumstances.

## TLS encryption

TLS encryption should be used to protect web interfaces at Volkis. Ideally unencrypted web interfaces will redirect to HTTPS.

All internet accessible TLS protected interfaces should be configured as per [NIST 800-52r2](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-52r2.pdf). This includes:

- Disabling TLS versions prior to v1.2.
- Completely disabling SSL.
- Configuring only strong encryption protocols.

The systems may use LetsEncrypt and Cloudflare certificates.

## Acceptable encryption algorithms

Only approved cryptographic algorithms and protocols shall be used, including but not limited to:

- AES (Advanced Encryption Standard)
- RSA
- DSA (Digital Signature Algorithm)
- ECC (Elliptic Curve Cryptography)
- SHA3

Outdated or vulnerable algorithms such as DES, RC4, and MD5 must never be used to protect data.

## Key management

Keys used for encryption must be protected at all times. When creating a new system for encryption, you must consider:

- Where the keys will be stored.
- Who has access to the keys.
- How often keys should be rotated.
- How keys are backed up.
