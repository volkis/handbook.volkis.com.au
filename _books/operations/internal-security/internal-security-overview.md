---
title: Internal security overview
owner: Matthew Strahan
last_reviewed_on: 2024-06-22
---

Volkis is a cyber security organisation that seeks to improve the security of our customers and the wider community. Our part in this is protecting ourselves and the data and access that we are entrusted with.

## Internal security objectives

To achieve this, we need to:

- **Minimise the risk of the compromise of sensitive information enstrusted to us**: We need to place appropriate controls on the storage, processing, and transfer of sensitive information.
- **Ensure our services maintain the security of our clients**: Our consultants are often placed in a privileged position in our clients infrastructure. We need to maintain the security and integrity of the systems and tools we use, and use sensible precautions when performing activities that may have detrimental effects for client infrastructure.
- **Support Volkis operations and continued service delivery**: Our personnel are supported by our IT systems. The continued availability of our infrastructure is needed to ensure the continuity of our service.

## Key principles

When implementing information technology systems in Volkis, we follow these principles:

- **Least privilege**: People should only have access to what they need for their work and no more.
- **Segregation of access and duties**: Where possible, actions that affect security should require approval.

## Roles and responsibilities

The following roles have been defined within Volkis:

- **Information Security Officer**: Matthew Strahan

## How Volkis handles risk

Volkis attempts to minimise the risk of compromise to our clients, our client's data, and the data of their customers.

We undertake risk around our operations in order to be innovative and improve our efficiency.

## Laws, regulations, and compliance

Volkis have obligations under the following laws:

- **Australian Privacy Act**: We are not a consumer facing organisation and do not store Personally Identifiable Information (PII) of customers. We may store workplace information of our client contacts. While testing, we may encounter PII on client systems. This PII must not be retained as per our data retention policy.

We seek to comply with the following frameworks:

- **ISO 27001**: We seek to maintain an ISMS that complies with the ISO 27001 standard. This is both for internal security, federal government, and client requirements.
- **Essential 8**: The Essential 8 maintains a baseline set of technical security and process requirements. We need to comply for federal government and client requirements.

### Industry partners

Volkis maintains relationships with industry bodies and special interest groups. This includes the Australian Signals Directorate (ASD), and state security bodies.
