---
title: Data classification and protection policy
owner: Matthew Strahan
last_reviewed_on: 2024-06-22
---

## Classification levels

Data at Volkis is classified according to the following classification levels:

- **Client Confidential**: Client data that is entrusted to Volkis, including:
  - Testing data
  - Data supplied confidentially to Volkis by clients
  - Reports and other outputs of Volkis work for clients, including report generation files
  - Exploitation information
  - Credentials for client systems including testing credentials
- **Internal**: Data that is considered confidential to Volkis including payroll, financial data, operational data.
- **Open**: Any other data.

## Authorised storage locations

Below is a table for allowed storage locations for data of each classification level. Note that even if there's a tick there, it doesn't necessarily mean you can store all data of that classification level. For instance, don't go storing payroll in Privbook. Exceptions may be allowed on a case-by-case basis.

✘: Must not store this data here.
✔: May be allowed to store this data.

| Storage location | Client Confidential | Internal | Open |
| ---------------- | ------------------- | -------- | ---- |
| Sharepoint - project folders | ✔ | ✘ | ✔ |
| Sharepoint - Operational folder | ✘ | ✔ | ✔ |
| Onedrive folders | ✘ | ✘ | ✔ |
| User systems outside of above folders | Temporarily during projects | ✘ | ✔ |
| Xero | ✘ | ✔ | ✔ |
| Trello | ✘ | ✔ | ✔ |
| Qwilr | ✘ | ✔ | ✔ |
| Pipedrive | ✘ | ✔ | ✔ |
| Resource Guru | ✘ | ✔ | ✔ |
| Volkis Handbook | ✘ | ✘ | ✔ |
| Volkis Privbook | ✘ | ✔ | ✔ |
| Physical media inc paper and USB sticks | ✘ | ✘ | ✔ |

## Security controls that must be applied

The following general guidelines must be followed for all **Client confidential** and **Internal** data:

- Storage of this data must be minimised. Data must be removed when it is no longer in use.
- Data must be encrypted when at rest.
- Data must be encrypted in transit.
- Data must only be accessible to those with a business need.
- Appropriate labelling must be applied. This could be through:
  - Headers or footers
  - Including them in project folders
- Sensitive data should be masked where prudent, for instance when being displayed in public user interfaces.

## Redacting sensitive data for open usage

When reclassifying data as Open, all sensitive data must be redacted. The following guidelines must be followed when redacting data:

- Do not simply hide text by changing its highlight color to black. This method only obscures the text visually; selecting the text can reveal the information.
- Avoid changing the font color to match the background color. This technique also fails to fully secure the information, as the text can still be revealed by selecting it.
- Do not use graphical elements, such as black boxes, to cover sensitive text. These elements can often be moved or deleted, and the underlying text can sometimes be selected or revealed.
- Prevent sharing documents with reversible history. Ensure that redactions cannot be reverted or undone by others. Use secure redaction methods that permanently remove the information.
- Avoid blurring images. Image recovery software can sometimes restore blurred content. Use proper redaction methods to ensure that sensitive information is completely and permanently removed.

## Personally Identifiable Information

The storage and transmission of Personally Identifiable Information (PII) must be minimised at Volkis. This should include:

- Meeting the requirements of the Australian Privacy Act.
- Removing PII that is not needed for business purposes.
- Ensuring screenshots that contain PII are redacted prior to putting them in reports.
- Scrubbing PII from testing data where possible.


