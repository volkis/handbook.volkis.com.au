---
title: Acceptable usage policy
owner: Matthew Strahan
last_reviewed_on: 2024-06-22
---

This policy lays out what you can and can't do as a user with Volkis owned systems.

## Volkis laptops and devices

Volkis provided laptops and devices must not be used in the following ways:

- Don't use it to break the law.
- Don't use it to hack or attack people without explicit permission and sign-off.
- Don't use it to harrass, bully, threaten or intimidate. 

## Sharepoint, onedrive, and Volkis owned cloud hosted systems and storage

The online Volkis cloud hosted storage is for Volkis stuff. Please don't upload:

- Sensitive personal data or PII
- Illegal or copyright infringing content
- Offensive content
- Content designed to bully, threaten, or intimidate

## Monitoring

As part of our internal security, we actively log and monitor user activities. This may include:

- **Data collected by EDR**: Programs run and times; Malware detected.
- **Data collected by Microsoft 365**: Logins; Changes made to cloud storage.
- **Data collected by Gitlab**: Changes committed to repositories.
