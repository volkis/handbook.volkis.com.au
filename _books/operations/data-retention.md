---
title: Data Retention
---

## Overview

This policy governs the minimum length of time data is retained at Volkis. These retention lengths will direct secure deletion at Volkis.

Retention length may differ based on client and contractual requirements.

## Data retention lengths

| Data type | Retention length |
| --- | --- |
| Data marked as Personally Identifiable Information collected during client engagements | Not retained |
| Penetration testing data | 1 year |
| Client compliance supporting documentation | 2 years |
| Security testing Reports | 2 years |
| Compliance reports Reports | 2 years |
| Client contact information | Indefinite |
| Project management data | Indefinite |
| Client and partner signed legal documentation | Indefinite |
| Client proposal acceptance | Indefinite |
| Tax and superannuation records for employees | 5 years |

## Legal and regulatory references

- [ATO record keeping rules for businesses](https://www.ato.gov.au/business/record-keeping-for-business/overview-of-record-keeping-rules-for-business/)