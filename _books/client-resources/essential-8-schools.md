---
title: Essential 8 Guidance for Schools
owner: Matthew Strahan
last_reviewed_on: 2024-09-18
---

Independent schools have been looking for the ASD Essential 8 standard to improve the security of their systems and to protect their students. This standard offers a baseline of security requirements that the ASD thinks is suitable for all organisations.

Schools have rather unique and specific security requirements, though, so the Essential 8 maturity levels often don't quite match up well to what a school should implement. There are controls from Maturity Level 1 that schools might not want to implement, and there are controls from Maturity Level 3 that schools might want to prioritise.

We have put together a collection of answers to common questions and solutions to common problems we see when helping schools use the Essential 8 to increase their security. The intention of this guidance is to be collaborative, so if you have any problems or solutions that could be added here, please let us know by either putting an issue in our [Handbook Gitlab](https://gitlab.com/volkis/handbook.volkis.com.au) or [contacting us directly](https://www.volkis.com.au/contact/).

This page is still a work in progress. It is general advice only and may not be appropriate for your school.  [Contact us](https://www.volkis.com.au/contact/) for more specific advice. [For official documentation refer to the ASD website](https://www.cyber.gov.au/resources-business-and-government/essential-cyber-security/essential-eight).

## Brief overview of the standard (skip this if you already know)

The Essential 8 ([available here](https://www.cyber.gov.au/resources-business-and-government/essential-cyber-security/essential-eight)) is a baseline set of security controls defined by the Australian Signals Directorate (ASD). It is based on their experience in responding to real world events and provides practical steps to mitigating these sorts of incidents.

The standard itself is split into eight strategies. They are:

- Application control
- Patch applications
- Patch operating systems
- User application hardening
- Configure Microsoft Office macro settings
- Restrict administrative privileges
- Multi-factor authentication
- Regular backups

Each of the eight strategies in the Essential 8 has three maturity levels. These levels define not just whether the controls are in place, but how effective they are likely to be at preventing and detecting attacks.

The maturity levels expand on each other. To meet the requirements of Maturity Level 2, for example, the requirements of Maturity Level 1 must be fulfilled as well as the additional requirements for Maturity Level 2. To meet Maturity Level 3 for all 8 strategies of the standard, 152 requirements must be met in total.

The only exception (as of 2024) to this is the **Patch operating systems** strategy. There are no Maturity Level 2 requirements for this strategy, which means if you get Maturity Level 1 you automatically get Maturity Level 2.

Generally, schools don't have specific compliance requirements around Essential 8. Instead schools pick and choose from the standard to have practical and measurable increases to their cyber security systems and practices.

## Overall guidance

### How should we assess ourselves against the Essential 8?

There's no certification program for the Essential 8, instead you generally self assess or get a third party to give you a check over. While there's [assessment guidance from the ASD](https://www.cyber.gov.au/resources-business-and-government/essential-cyber-security/essential-eight/essential-eight-assessment-process-guide), it's high level and really up to yourselves how to follow it.

As with all assessments it's best to strike a balance between effectiveness and cost. If you don't do many checks you're probably missing a bunch of edge cases. If you do too much you're spending time and resources assessing yourself that might be better spent elsewhere.

We find the following activities a pretty good middle ground for schools:

- **Workshop**: Get together the main IT administrators and go through the standard, talking through compliance, potential edge cases, and expectations.
- **Documentation review**: Ensure that Essential 8 controls are appropriately addressed in the policies, processes, and standards of the school.
- **SOE review**: Actively check a sample system with the school's standard operating environment, ensuring that patches are up to date, macros don't run, and applications are hardened.

We also feed in results from penetration testing and other security reviews that are performed. If the penetration testing, for instance, identified missing patches then that's a good indication that the **Patch applications** and **Patch operating systems** processes aren't working as effectively as you might think.

### Should we include student laptops in scope?

It's your choice, but I usually say "no". There are a lot of requirements that are very suitable for student laptops (backups, turning off Microsoft Office macros, application control), but there are also requirements that are more difficult, especially for laptops issued to younger kids.

I put together another guide around securing laptops in our blog.

### Terminology

When applying to schools we occasionally need to stretch terminology. This is how I apply specific terms to schools:

- **Customer**: School kids, parents and guardians of kids.
- **Customer services**: Systems like TAS that are logged in by students and systems that are logged into by parents and guardians.

### Do I really need to contact ASD when an incident occurs?

Reporting to the ASD is part of Maturity Level 2 for three of the strategies:

- Multi-factor authentication
- Restrict administrative privileges
- Application control
- User application hardening

This is quite annoying for those who don't want to include it in their incident response plans but still want to achieve Essential 8 compliance. For my assessments on these requirements I usually look to identify whether there's a contractual or regulatory requirement for including it, otherwise I mark it as N/A.

That said, I still recommend including it. There's no harm in alerting the ASD in the event of an incident - your information will be kept confidential. They can also provide enormous assistance when an incident occurs, from active threat intelligence to direct incident response support. `1300 CYBER1` is a very easy number to remember.

### How should I report to the board on Essential 8 compliance?

We have our sample report [publicly available](https://handbook.volkis.com.au/assets/doc/Volkis%20-%20Sample%20report%20-%20Essential%208%20Maturity%20Model%20Assessment%20May%202022.pdf). Feel free to take some of the reporting ideas from there for your own reporting purposes. In particular, we have found the following diagram particularly effective for showing compliance:

![Essential 8 radar chart](/assets/img/essential-8-radar.png)

Clients have mentioned it as a very easy and visual way of showing achieved maturity levels.

## Patching applications and operating systems

I would usually target Maturity Level 2 for schools for these two strategies.

Note that with the 2024 release of Essential 8, there are no Maturity Level 2 specific requirements for **Patch operating systems**.

### What would be an automated method of asset discovery?

This requirement allows a little bit of creativity on how it's implemented. There are a few mechanisms:

- Using a network scanner, often built into vulnerability scanning tools.
- Using network infrastructure, for example Unifi gear has a list of connected hosts.
- Using a dedicated asset discovery tool such as RunZero.

The main considerations here from our side are:

- If there's a new system that connects to the network, will it be scanned for vulnerabilities?
- If there's a new system that enrols into cloud based management such as Microsoft 365, will that be included as well?

### What kind of vulnerability scanners are acceptable?

The two main tactics we see for vulnerability scanners are:

- Host based scanners like Microsoft Defender Vulnerability Management.
- Network based scanners such as Qualys, Rapid7, or Tenable Security Center.

The main disadvantage of Microsoft Defender Vulnerability Management is that you cannot easily scan the firmware of network devices and printers which stops you from achieving Maturity Level 3 in **Patch operating systems**.

## Application control

We find schools have mixed success at implementing **Application control** usually for legacy reasons - teachers have gotten used to being able to install anything on their devices so they complain if they have that taken away. This makes implementing multi-factor authentication sometimes more of a change management challenge than a technical one.

That said, application control is probably the most effective security control for schools there is. It will stop malware from running on school systems entirely.

Once an application control system is in place, we find that schools tend to be able to comply with Maturity Level 2 or Maturity Level 3 of the standard, depending on the technology chosen. An all-in-one solution like Airlock Digital can get Maturity Level 3 in one swoop, whereas schools using an older technology like Microsoft Applocker can struggle to implement the Microsoft blocklists effectively without additional scripting.

## Multi-factor authentication

Schools generally target Maturity Level 1 for **Multi-factor authentication** (MFA). The main sticking point for schools in Maturity Level 2 is requiring the use of MFA for logging into systems and for online customer portals.

### How can MFA work for user devices?

Usually through biometrics using a system such as Windows Hello for Business, FaceID or TouchID or by using a secondary device such as Passkeys or even a Yubikey.

It might sound a bit mind bending, but biometric login to a device is by nature multi-factor authentication. When you're using biometrics you have:

- Something you are: The face/fingerprint.
- Something you have: The device itself.

### What does "phishing resistant" mean?

The 2024 updates to the strategy include the term "phishing resistant" but it's not really defined in the standard or accompanying guidance. What we believe this is referring to is multi-factor authentication mechanisms which are resistant against proxy based phishing.

The most common attack when phishing for accounts protected by MFA is to redirect the user to your own site and ask them for username and password, then when they're sent a token just ask for that as well. With the username, password, and token you can login on behalf of the user to the real website. Proxy based systems such as Evilginx can be used to streamline this process for the phisher and make it real time.

Phishing resistant MFA has some mechanism to validate that you're actually sending the token to the real site. This is usually through some kind of FIDO2 implementation such as Passkeys or Yubikeys or through operating system based mechanisms such as Windows Hello for Business.

Email tokens, SMS tokens, and mobile tokens are generally not "phishing resistant".

## Configure Microsoft Office macro settings

Nowadays we find limited usage of Microsoft Office macros in schools. For those that don't use macros at all it is rather routine to achieve Maturity Level 3. For those that still require some usage of macros within the school Maturity Level 1 is a good target.

[Microsoft has great (overly detailed) guidance](https://learn.microsoft.com/en-us/compliance/anz/e8-macro) for disabling macros.

## User application hardening

We generally target Maturity Level 1 for **User application hardening** for schools.

## Restrict administrative privileges

We generally target Maturity Level 1 for **Restrict administrative privileges** for schools.

## Regular backups

The overall intention of **Regular backups** is to protect the organisation against ransomware attacks. Unfortunately Education, like every sector, has been affected by ransomware attacks that target internal systems and encrypt data. Backups are a critical control for both mitigating the impact of ransomware attacks and recovering from the attack.

We would generally recommend targeting Maturity Level 2 or 3, depending on whether you can have immutable backups. The requirements are sensible and easy to follow in most cases.

### What are "business criticality and business continuity requirements" for schools?

The Essential 8 doesn't tell you specifically how often you should back up but instead tells you that you should back up according to "business criticality and business continuity requirements". I understand that a lot of schools won't have gone through business continuity programs and so defining what these requirements are can be a challenge.

When defining a backup schedule you should consider:

- **How much data (in time) can you lose?** This will help determine the Recovery Point Objective (RPO) and define the frequency of backups.
- **How quickly must each system be recovered?** This will help determine the Recovery Time Objective (RTO) and provide direction on what sorts of backups are acceptable.
- **How far back might you need to go to restore data?** This will help determine the retention schedule of backups.

When thinking this through you might also consider what would happen if systems would go down. For instance, if your IT infrastructure goes down, can you still run classes? Can kids get their homework? Can you run tests?

Ideally all of this should be documented in a "Business Continuity Plan", however usually for Essential 8 I just accept something sensible.

### How often should disaster recovery exercises be?

Like above, there's no strict rule on when disaster recovery exercises should be performed. Rather it should be something sensible depending on the risks.

Usually for schools this I would use a blanket rule of:

- Do a sample restoration from backups every quarter.
- Do a full restoration of a sample of systems (say a server and user device) every year.

### What are "backup administrator accounts"? Can they be Domain Admin?

The intention of this strategy is to make it so that if a ransomware group compromises the school network (Domain Admin for Active Directory or Global Admin for Microsoft 365) then they cannot also delete backups. If they get to the point where they can delete backups they can effectively make the attack impossible to recover from.

To achieve this the Essential 8 has a concept of "backup administrator" which is a separate account from Domain Admin or Global Admin. For most schools should be:

- Separate from Active Directory or Microsoft 365.
- Have a random password that is not stored in a location accessible within Active Directory (for instance an internal password manager).
- Be protected with MFA that, again, is not reliant on Active Directory connected devices.
- Ideally, unable to delete backups so that even if it's compromised you can still recover from a ransomware attack.
