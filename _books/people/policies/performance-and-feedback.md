---
Title: Performance and Feedback
owner: Matthew Strahan
last_reviewed_on: 2024-09-22
---

## Ongoing performance

Non-formal ongoing feedback should generally be used as a primary approach for improving performance. In general, if someone doesn't know where they stand until some kind of yearly "annual review" then the manager doesn't have good communication.

Effective feedback should be:

- **Specific**: A vague "you did bad" is not very helpful for improving performance. Feedback should relate to actions and activities that should be taken.
- **Timely**: The more time that goes by between an action and feedback on the action, the harder it is to improve. Ensure that any feedback is given as soon as possible.
- **Constructive and actionable**: Focus on what the person should do, rather than what they should avoid.
- **Focused on actions, not on the person**: Never relate actions to personality or identity. Instead, focus on how actions or behaviour can be improved.

## Performance improvement plans

Performance improvement plans (or "PIPs") are formal, documented approaches for improving the performance of personnel. At Volkis they should only be used where an employee fails to meet the expectations of their role and there are concerns about the ongoing ability to meet those expectations. The plan should:

- Document clear objectives with measurable and achievable outcomes.
- Be individual to the employee.
- Include regular check-ins and ongoing feedback (the above section shouldn't be neglected!)

The Australian government has [built a sensible checklist for building such a plan](https://www.fairwork.gov.au/sites/default/files/migration/766/setting-up-a-performance-system-checklist.pdf). This should be used to ensure the plan is fair.


