---
Title: Volkis People Benefits
owner: Matthew Strahan
last_reviewed_on: 2024-09-22
---

When working at Volkis you are provided with a bunch of benefits.

**It is important that you read this all carefully** so you can take advantage of these benefits. This will probably be updated as we think of more things to do.

- **Training**: Learning and education is critical for ensuring Volkis services remain as best as they can be. You should be assigned a training budget that can be used for formal training courses, conferences, certifications, casual learning, and associated flights, accommodation, and travel costs.
- **Swag**: As part of Volkis you will be provided with a bunch of swag. Initially this will include a Volkis logo engraved glass and stickers. We will likely provide you with an annual custom designed Volkis T-shirt and employee-only additional clothing item (for instance a hoodie, jumper, or jacket).
- **Birthday off**: Each year full-time employees can have an additional day of work off for their birthday. This can be taken either on your birthday or up to two weeks either side of your birthday.
- **Right to disconnect**: We seek to ensure that you have good work-life balance while working at Volkis. As part of this, you will not be expected to answer any emails, phone calls, or other notifications outside of working hours or otherwise scheduled work.
- **Presentation days and Volkis events**: Volkis will periodically hold events for the organisation that you have the option of attending. When this occurs, your flights, accommodation, and additional travel costs will be paid for by Volkis according to our expenses policy.
