---
Title: Disciplinary Process
owner: Matthew Strahan
last_reviewed_on: 2024-09-22
---

This process covers dealing with negligence, misconduct, and performance improvement in Volkis employees.

We're aware that sometimes things go wrong and sometimes mistakes happen. We attempt to make sure that we consider our value of assuming good intentions. If there's an honest once-off mistake or unforeseeable negative consequences that occur, this should not be penalised. We should attempt to instead offer empathy and understanding when this happens.

However there will be times where there may need to be action taken. The following sections go through these circumstances and the actions that will be taken.

## Negligence or misconduct

This process should be followed when there is:

- A breach of policy.
- Workplace harrassment, violence, bullying, or other misconduct.
- Negligence or misconduct that leads to actual or potential harm.
- Unethical activity.

Prior to any response, we should consider the following:

- The nature of the offence.
- Whether it was intentional or accidental.
- Whether this is a repeated occurrance.
- Whether the employee was properly trained, supported, and informed.

**A severe violation, serious misconduct, or illegal activity may lead to immediate dismissal.**

Otherwise, the following steps may be taken. Considering the nature of the offense, steps might be skipped or repeated as necessary.

1. **Verbal warning**: Give a formal warning verbally in a meeting, Slack call, or Teams call. This should be explicitly noted to be a formal warning and documented separately.
2. **Written warning**: Give a written formal warning over email. The warning should clearly explain the actions taken to receive the warning, the consequences, and what may occur in the future if behaviour isn't corrected.
3. **Active investigation**: The employee is taken off active duties while a formal investigation occurs. This should include interviewing the employee and other personnel involved as well as looking at evidence.
4. **Dismissal**: Employment is immediately terminated and access to Volkis systems and data is cut.

## Performance

See the page on [performance and feedback](/people/performance-and-feedback/)
