---
Title: People Policies
owner: Matthew Strahan
last_reviewed_on: 2024-09-19
---

- [Volkis Benefits](/people/policies/benefits/)
- [Disciplinary Process](/people/policies/disciplinary-process/)
- [Performance and Feedback](/people/policies/performance-and-feedback/)



