---
title: Biographical data sheets
---

## Overview

This page includes biographical data sheets for key personnel in Volkis.

The [Industry Presentations](/people/industry-presentations) page showing presentations from our team is a companion to this page.

## Matthew Strahan - Managing Director

18 years of dedicated, cyber security experience in designing security strategies, security architecture, implementing cyber security frameworks and standards, developing policy, procedure, standards, and processes, technology selection and implementation, incident response, and penetration testing. Carries industry qualifications including GAICD, CISSP, CISM, CISA, CGEIT, and OSCP, holds a Bachelor of Computer Science and Masters of Business Administration.

13 years of team leadership and management experience as the leader of operations and principal consultant, including leading internal IT, including support, IT transformations and an ISO27001 implementation. Led the implementation of internal systems and process improvement.

Currently serves as Managing Director of Volkis.

Available on the following social media sites:

- [LinkedIn](https://www.linkedin.com/in/matthew-strahan/)
- [Twitter](https://twitter.com/matt_strahan/)

### Employment history

| Company          | Role                       | From |   To    |
| ---------------- | -------------------------- | :--: | :-----: |
| Volkis           | Managing Director          | 2019 | Present |
| Content Security | Senior Security Strategist | 2019 |  2019   |
| Content Security | Operations Manager         | 2017 |  2019   |
| Content Security | Principal Consultant       | 2012 |  2017   |
| Securus Global   | Security Consultant        | 2007 |  2011   |

### Education

| Institution          | Degree                 | Obtained |
| -------------------- | ---------------------- | :------: |
| Macquarie University | MBA                    |   2021   |
| University of NSW    | BSc (Computer Science) |   2007   |

### Certifications

- Graduate of AICD Company Directors Course (GAICD)
- Certified Information Security Auditor (CISA)
- Certified Information Security Manager (CISM)
- Certified in Governance of Enterprise IT (CGEIT)
- Certified Ethical Hacker (CEH)
- Certified Information Systems Security Professional (CISSP)
- Offensive Security Certified Professional (OSCP)

### Key skills

- Experienced in business and technical security.
- Penetration testing, including internal, external, wireless, web app, desktop app.
- Business security, including security architecture, governance, management, policy, procedure, and framework.
- Credit card security, including PCI DSS.
- General IT, including transformation, governance, programming, software engineering.
- Presented at industry bodies such as CPA Forum, Power Housing Risk and CFO meetings, LGASA.

## Alexei Doudkine - Offensive Director

Penetration tester, hacker and security consultant with a decade of experience in building security systems and providing bespoke security consultancy with empathy. Worked to build and lead multiple teams and service offerings including penetration testing, security consulting, IR and security training.

Built the "Hands-on Hacking" security education course, teaching members of the NSW Police and Department of Justice around hacking techniques.

Takes great pride in helping students and newcomers to the industry get into the cyber security field through workshops, online content and CV reviews. E.g. [UTS CSEC workshop](https://youtu.be/Jpcj8eyFpTA)

Created hacking tools for security professionals including [RidRelay](https://github.com/skorov/ridrelay).

Outside of infosec, loves taking Volk the siberian husky 🐺 for walks, modding cars 🚗, rock climbing 🧗‍♂️, movies 🎥, and games 🎮.

Available on the following social media sites:

- [LinkedIn](https://www.linkedin.com/in/alexei-doudkine/)
- [Twitter](https://twitter.com/skorov8)

### Employment history

| Company          | Role                 | From |   To    |
| ---------------- | -------------------- | :--: | :-----: |
| Volkis           | Offensive Director   | 2019 | Present |
| Content Security | Principal Consultant | 2017 |  2019   |
| Content Security | Security Consultant  | 2013 |  2017   |
| Pacom Systems    | Software Engineer    | 2010 |  2013   |

### Education

| Institution       | Degree                 | Obtained |
| ----------------- | ---------------------- | :------: |
| University of NSW | BSc (Computer Science) |   2010   |

### Certification

- Offensive Security Certified Professional (OSCP)
- Offensive Security Experienced Penetration Tester (OSEP)
- CREST Registered Tester (CRT)
- GIAC Certified Forensic Examiner (GCFE)
- Certified Information Systems Security Professional (CISSP) (lapsed)
- Payment Card Industry - Qualified Security Assessor (PCI-QSA) (lapsed)

### Key skills

- Red team, Penetration testing, including internal, external, wireless, web app, mobile app
- Social engineering and physical security, including manipulating security badges and access control systems
- Presenting, including creating full security education courses
- Infrastructure development and systems administration
- Security design and architecture
- Business security, including payment card industry, risk assessments

## Joshua Rynan - Senior Security Consultant

A over decade of IT security experience, half of which being dedicated to penetration testing.
Expertise in setting up and running enterprise vulnerability management programs.
Experience running an associate penetration tester program, upskilling new pentesters from various degrees of industry experience.
Spends too much time thinking about how to write better reports.

Available on the following social media site:

- [LinkedIn](https://www.linkedin.com/in/joshua-rynan/)

### Employment history

| Company          | Role                       | From |   To    |
| ---------------- | -------------------------- | :--: | :-----: |
| Volkis           | Senior Security Consultant | 2021 | Present |
| CyberCX          | Penetration Tester         | 2019 |  2021   |
| Content Security | Penetration Tester         | 2016 |  2019   |
| IBM              | Vulnerability Scan Analyst | 2011 |  2015   |

### Education

| Institution           | Degree                       | Obtained |
| --------------------- | ---------------------------- | :------: |
| Federation University | BITC (Professional Practice) |   2010   |

### Certifications

- Zero-Point Security Certified Red Team Leader (CRTL)
- Zero-Point Security Certified Red Team Operator (CRTO)
- Offensive Security Certified Professional (OSCP)
- Spectre Ops Adversary Tactics: Red Team Operations (AT:RTO)

### Key skills

- Penetration testing, including internal, external, wireless, web app and social engineering.
- Making memes in MS Paint.

## David Chadwick - Senior Security Consultant

Over fifteen years of IT security experience, of which six was dedicated to penetration testing.
Expertise in setting up and running enterprise vulnerability management programs, team leadership, process engineering and documentation.

Available on the following social media site:

- [LinkedIn](https://www.linkedin.com/in/chadwickdavid/)

### Employment history

| Company           | Role                                         | From |   To    |
| ----------------- | -------------------------------------------- | :--: | :-----: |
| Volkis            | Senior Security Consultant                   | 2021 | Present |
| CyberCX           | Senior Security Consultant - Team Lead       | 2020 |  2021   |
| Sense of Security | Senior Security Consultant                   | 2020 |  2020   |
| Content Security  | Senior Security Consultant                   | 2019 |  2020   |
| Content Security  | Security Consultant                          | 2015 |  2019   |
| IBM               | Vulnerability Scan Analyst                   | 2012 |  2015   |
| IBM               | Intel and Remote Access Security Team Leader | 2010 |  2012   |
| IBM               | Remote Access Security Dispatcher            | 2009 |  2010   |
| IBM               | Remote Access Security Administrator         | 2008 |  2009   |
| IBM               | Security Operations Administrator            | 2007 |  2008   |
| IBM               | Customer Service Consultant                  | 2006 |  2007   |

### Education

| Institution           | Degree                       | Obtained |
| --------------------- | ---------------------------- | :------: |
| Federation University | BITC (Professional Practice) |   2008   |

### Certifications

- Offensive Security Certified Professional (OSCP)

### Key skills

- Penetration testing, including internal, external, wireless, web app and social engineering.
- Experienced in business and technical security

## Finn Foulds-Cook - Senior Security Consultant

Over 8 years of experience engaging in cybersecurity at conferences or monthly meetups. Interested in infrastructure, Kubernetes, rock climbing 🧗‍♂️, gaming and programming.

Available on the following social media sites:

- [LinkedIn](https://www.linkedin.com/in/finn-foulds-cook-509ba7144/)
- [Twitter](https://twitter.com/f3rn0s)
- [Blog](https://www.f3rn0s.xyz)

### Employment history

| Company    | Role                       | From |   To    |
| ---------- | -------------------------- | :--: | :-----: |
| Volkis     | Senior Security Consultant | 2024 | Present |
| Volkis     | Security Consultant        | 2022 |  2024   |
| Red Cursor | Junior Security Consultant | 2020 |  2022   |

### Education

| Institution                   | Degree                                  | Obtained |
| ----------------------------- | --------------------------------------- | :------: |
| University of New South Wales | Bachelor of Computing and Cybersecurity |   2020   |

### Certifications

- Offensive Security Certified Professional (OSCP)
- Certified Kubernetes Administrator (CKA)

### Key skills

- Penetration testing including: internal, external, wireless and web applications.
- Programming experience in Rust, Python, Java, TypeScript, JavaScript and Go.
- Kubernetes.
- Acting as a human [monkey typewriter](https://en.wikipedia.org/wiki/Infinite_monkey_theorem) and discovering obscure bugs.

## Andy Wallis - Senior Security Consultant

Over twenty years of IT experience (that makes me feel old), with the last seven years as a professional penetration tester.
Working as a system administrator for many years along side developers has given me a unique perspective and empathy for the challenges faced when securing both networks and applications.

Available on the following social media site:

- [Twitter](https://twitter.com/m0nk3yb0i80)

### Employment history

| Company          | Role                                     | From |  To  |
| ---------------- | ---------------------------------------- | :--: | :--: |
| Volkis           | Senior Security Consultant               | 2022 | Present  |
| Tesserent        | Senior Security Consultant               | 2020 | 2022 |
| NCC              | Senior Security Consultant               | 2020 | 2020 |
| Content Security | Offensive Consultant                     | 2019 | 2020 |
| Alcorn Group     | Security Consultant                      | 2017 | 2019 |
| Content Security | Security Consultant                      | 2015 | 2017 |
| EY               | Senior Security Consultant               | 2015 | 2015 |
| Thales           | Senior System Administrator              | 2014 | 2014 |
| Olam             | Senior Network Administrator             | 2013 | 2013 |
| Caterpillar      | Network Administrator                    | 2011 | 2013 |
| ZBI Quan         | IT Infrastructure Specialist             | 2010 | 2011 |
| Caterpillar      | Network Administrator                    | 2008 | 2010 |
| Suncorp          | System Security Access Administrator     | 2007 | 2008 |
| Odyssey Gaming   | System Administrator / Software Engineer | 2002 | 2007 |
| Bounty Limited   | Software Engineer/Onsite Support         | 2001 | 2002 |

### Education

| Institution | Qualification              | Obtained |
| ----------- | -------------------------- | :------: |
| Tafe NSW    | Dp. PC and Network Support |   2001   |

### Certifications

- Offensive Security Wireless Professional (OSWP)
- eLearnSecurity Certified Professional Penetration Tester (eCPPT)

### Key skills

- Penetration testing, including internal, external, wireless, web and mobile application.

## Nathan Jarvie - Security Consultant

I have over ten years of IT experience. Most of that time was spent working as, and leading a team of systems and network administrators for an MSP, which allowed me to work with a variety of technologies and people and across many industries. I am experienced with Mac, Windows and Linux systems and environments.

I moved into a career in penetration testing in 2021, and seem to be determined to get every certification that is available.

I moonlight as a Latin American and Ballroom dancing instructor during the week. I also have achieved my Black Belt in Taekwondo, and regularly assist in classes.

Available on the following social media sites:

- [LinkedIn](https://www.linkedin.com/in/nathan-jarvie/)
- [Twitter](https://twitter.com/nathanjarvie)


### Employment history

| Company | Role                                | From |  To  |
| ------- | ----------------------------------- | :--: | :--: |
| Volkis  | Security Consultant                 | 2022 | Present  |
| Volkis  | Associate Security Consultant       | 2021 | 2022 |
| Mac Aid | Technical Manager                   | 2020 | 2021 |
| Mac Aid | Senior Systems and Network Engineer | 2012 | 2021 |

### Education

| Institution | Qualification              | Obtained |
| ----------- | -------------------------- | :------: |
| RMIT        | Dp. Information Technology |   2012   |

### Certifications

- HTB Certified Bug Bounty Hunter (CBBH)
- Altered Security Certified Azure Red Team Professional (CARTP)
- Altered Security Certified Red Team Expert (CRTE)
- Zero-Point Security Certified Red Team Operator (CRTO)
- eLearnSecurity Web Penetration Tester (eWPT)
- Offensive Security Certified Professional (OSCP)
- TCM Security Practical Network Penetration Tester (PNPT)


### Key skills

- Penetration testing, including internal, external, wireless, and web applications
