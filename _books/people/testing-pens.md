---
title: So you want to test pens?
owner: Josh
last_reviewed_on: 2023-12-14
---

Sit down, stay awhile and listen.

The following is a high level overview for those asking "I would like to be a Penetration Tester (Pentester) but how do I get into the industry? How do I get started?".

Here is a great [blog](https://www.volkis.com.au/blog/sysadmin_to_pentester_part_1/) to start with, it was written by my colleague Nathan who moved from being a System Admin to a Pentester. I understand that it implies you have already been in the IT industry but I think it still gives a good overview of the journey to becoming a penetration tester.

## "The five years experience for an entry level position" paradox

Breaking into computer security is hard, it is even harder to get into penetration testing. The following are some recommendations for building skills and certifications that won't break your bank, help get your CV past a recruiter and can help demonstrate your skills during interviews.

I will break this section into two parts, infrastrucure testing (internal networks and Internet facing infra) and web application testing. These tend to be the two main areas that you focus on when starting out as a pentester.

### Infrastructure

I would recommend reading the following [write up](https://www.volkis.com.au/blog/ad-hacking-speedrun/) on hacking Active Directory, done by my Director Alexei. It will give you some insight on the types of tools, techniques and practicalities of internal testing. 

**Courses**

The following are a list of courses / providers that I recommend for beginners to start on building their familiarity with infrastructure tools and techniques:
  
    - Metasploit Unleashed, nice easy and free guide to using Metasploit Framework: <https://www.offensive-security.com/metasploit-unleashed/>
    - TCM Security, these dudes do amazing courses at amazing prices, they are practical, up to date and teach you a lot. There is also several certifications if you want to pursue certifications: <https://academy.tcm-sec.com/courses>
    - Their PNPT course is a great place to start and covers infra and web testing, at time of writing the course can be streamed for free: <https://academy.tcm-sec.com/p/pnpt-live>

**Tools**

Here is a quick list of tools that you can start to look at that are useful for instrastructure
testing:

  - Nmap, The worlds most popular port scanning tool: <https://nmap.org/>
  -	CrackMapExec (CME), An amazing tool for auditing Windows AD networks. If you have a local admin account on a machine you can easily dump credentials and do other fun things 😉: <https://wiki.porchetta.industries/>
    - **Note**: This is no longer supported and was superceded by NetExec: <https://github.com/Pennyw0rth/NetExec>
  -	Mimikatz, dump passwords from Windows memory: <https://github.com/ParrotSec/mimikatz>
  -	Impacket Library, collection of awesome Python tools to tear apart Windows domains: <https://www.secureauth.com/labs/open-source-tools/impacket/>
  -	Responder, capture NTLM and NTLMv2 hashes on Windows Domains: <https://github.com/lgandx/Responder>

**Resources**

The following are some nice places to get examples of techniques and tools:

  -	IRedTeam, this contain a great list of tools and techniques sorted by attack stage and then by service being attacked: <https://www.ired.team/offensive-security/initial-access>
  -	NTLM Relaying Windows networks: <https://byt3bl33d3r.github.io/practical-guide-to-ntlm-relaying-in-2017-aka-getting-a-foothold-in-under-5-minutes.html>

### Web Applications

**Courses**

The following are a list of courses / providers that I recommend for beginners to start on building their familiarity with infrastructure tools and techniques:

  -	Burp Academy, really well put together courses for different classes of web application vulnerabilities, hands on and free. Even give you answers you can refer to when you get stuck (I often do): <https://portswigger.net/web-security>
  - TCM Academy, again has solid reasonably prices web courses as well: <https://academy.tcm-sec.com/courses>

**Tools**

  -	Burp Suite, the one stop shop for web app security testing, has a free community edition: <https://portswigger.net/burp/communitydownload>
  -	Sublist3r, used for subdomain enumeration: <https://github.com/aboul3la/Sublist3r>
  -	Gobuster, used for subdomain and directory enumeration: <https://github.com/OJ/gobuster>
  -	SQLMap, used to hunt for and automatically exploit SQL injection vulnerabilities: <https://github.com/sqlmapproject/sqlmap>
  - SecLists, A collection of multiple types of lists including, include usernames, passwords, URLs, sensitive data patterns, fuzzing payloads: <https://github.com/danielmiessler/SecLists>
  -	OWASP Juiceshop, Free vulnerable web app that is easy to setup so you can practice techniques and tools: <https://github.com/juice-shop/juice-shop>

**Guides**

**Note**: There are no guides, web apps are an arcane science and I dont care for them.

## What are some good resources for security in general?

I enjoy watching the videos of the following people:

- <https://www.youtube.com/@_JohnHammond>
- <https://www.youtube.com/@ippsec>
- <https://www.youtube.com/@TCMSecurityAcademy>
- <https://www.youtube.com/@LiveOverflow>

Hope that gives you a bit to sink your teeth into.

If any of this piques your interest then we will make a pentester of you yet.
