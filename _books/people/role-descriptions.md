---
title: Internal Role Descriptions
owner: Alexei
last_reviewed_on: 2024-07-24
---

This page exists to shine a light on what roles we have at Volkis. They are by no means binding and should be considered a little "fuzzy". We never want to put people in boxes! Everyone has their niche and should have the freedom to thrive in their role while also doing the essentials.

## All roles

Roles and responsibilities:

- Assist and guide other consultants in their tasks;
- Act as a representative of Volkis in such a way that is in the best interest of the company;
- Aid in the growth, development and safety of Volkis and its employees;
- Ensure the security of Volkis and the data and access entrusted to us by clients;
- Additional ad-hoc duties as required by management.

## Associate Security Consultant

_You're just starting out in the wild world of infosec. Prepare to drink from the firehose of knowledge and learn very quickly._

Roles and responsibilities:

- All of the above roles and responsibilities;
- Learn, on-the-job, skills (listed below) that will help facilitate a promotion to Security Consultant;
- Assist clients with security consultation in the best interest of Volkis and the client.

Expected knowledge/skills:

- Foundational IT knowledge such computing, networking, and coding;
- Can explain complex security concepts in easy-to-understand terms;
- Basic understanding of business impact of a vulnerability (e.g. "operational disruption" rather than just "remote code execution").

## Security Consultant

_You're running your own projects, without much supervision. You've got a good grasp of your job and are dependable in the work that you do._

Roles and responsibilities:

- All of the above roles and responsibilities;
- Autonomously lead technical projects from start to completion, ensuring excellent quality of work and that client goals are achieved;
- Identify and handle unforeseen issues with some guidance;
- Self-driven, continued education (with assistance from Volkis) in a wide range of infosec topics;
- Help to keep internally-managed information repositories up-to-date.

Expected knowledge/skills:

- All of the above knowledge/skills;
- Proficient at most of Volkis's services (e.g. internal, external, wireless & web app pentesting);
- Understanding of a wide range of infosec concepts both related to Volkis services, and more generally;
- Can lead security discussions with both technical and non-technical people, such that they understand the concepts;
- Understanding of business-level security concepts and how infosec plays a part in the overall operations of client businesses.

## Senior Security Consultant

_You're actively involved in the success of Volkis. You're trusted to handle very important tasks without supervision. You understand infosec, both technical and the business side, very well._

Roles and responsibilities:

- All of the above roles and responsibilities;
- Proactively mentor, teach, and guide fellow team members;
- Identify and make "big picture" security recommendations to clients, even when they may not specifically be related to the project;
- Identify unforeseen issues, create a plan to handle the issue and see it is resolved, without the need for guidance. Step in and help others resolve issues when necessary;
- Ensure the continued improvement of Volkis services/tooling and suggest potential new services/tooling as the infosec industry grows (e.g. hacking, reporting, operations tools);
- Understand (potential) client goals/requirements, provide advice, and create a plan (e.g. in a proposal) for any security work required to meet those goals. Suggest alternatives when it's in the best interest of the client and Volkis. (e.g. during goal discovery and scoping meetings)

Expected knowledge/skills:

- All of the above knowledge/skills;
- Excellent at most of Volkis's services (e.g. all pentests and secure config reviews);
- Specialise in at least 1 advanced area of infosec (e.g. tool dev, Red Team ops, EDR bypass, malware/C2, phishing, web apps);
- Detailed understanding of business-level security concepts and nuances in a wide range of industries;
- Confidence and ability to Get Shit Done™️ with little initial information or ongoing guidance;
- Understanding of how their actions have affected a client's long-term security (i.e have services/recommendations geniunely improved security or not, and why).
