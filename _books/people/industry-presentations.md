---
title: Industry presentations
alexei_link: '[Alexei Doudkine](/people/biographical-data-sheets/#alexei-doudkine---offensive-director)'
matt_link: '[Matt Strahan](/people/biographical-data-sheets/#matthew-strahan---managing-director)'
nathan_link: '[Nathan Jarvie](/people/biographical-data-sheets/#nathan-jarvie---security-consultant)'
max_link: 'Max Alster-Caminer'
finn_link: '[Finn Foulds-Cook](/people/biographical-data-sheets/#finn-foulds-cook---security-consultant)'
---

Here is a list of all recorded presentations, talks, workshops, and training that our team has given.

## 2024

### How to brrrrrrrrrrrrr the making of a red team implant 

_Author: {{page.max_link }}_ ・ _Event: SecTalks SYD0x53_

**Slides**: <https://github.com/sectalks/sectalks/tree/master/talks/SYD0x53>

Recently, while working on a red team, one of my colleagues beautifully constructed a physical implant. However, it only supported Ethernet connectivity, and circumstances now demanded Wi-Fi capabilities. Subsequently, I was assigned the challenge of rapidly developing a physical implant with Wi-Fi capabilities, utilising materials that I had available or obtainable from your local JayCar and Officeworks. This presentation will take the audience through my journey of constructing this implant, showcasing each step and detailing the problems encountered along the way.

### What do you really need to authenticate?

_Author: {{page.matt_link }}_ ・ _Event: ComfyCon AU 2024_

**Video**: <https://www.youtube.com/watch?v=eefSKldlKaE>

To login to your account you need a username and password and maybe an MFA token if they feel like being secure. But can you use other pieces of information to change or bypass the need for a password or token? Could you, by going through a whole bunch of processes and what not, login with an email address or a drivers license? In this presentation I will go over authentication trees, mapping them out, and figuring out what can really be used to authenticate.

## 2023

### Kubernetes Hacking 101

_Author: {{ page.finn_link }}_ ・ _Event: Hack.Sydney 2023_

This workshop starts with the same presentation from _Help I want to attack Kubernetes_, but with an additional lab component that explores attacking a real-world cluster.

**Workshop guide:** [Link](https://f3rn0s-github-j01uk290f-f3rn0s.vercel.app/workshops/hcksyd-workshop/)

### Building a Cyber Resilient Business

_Author: {{ page.alexei_link }}_ ・ _Event: Holocron Cyber Webinar_

**Link:** <https://youtu.be/3UPZeJOu-mo>

Cyber threats continue to impact the productivity and desired outcomes of Australian organisations. This webinar presented a comprehensive understanding of how to build a cyber resilient business. Industry experts in the field of cyber security discussed strategies and best practices for protecting your organisation against data breaches.

### Help I want to attack Kubernetes

_Author: {{ page.finn_link }}_ ・ _Event: SecTalks Brisbane June_

**Slides:** [Link](https://github.com/f3rn0s/public-slides/blob/main/Help%20I%20want%20to%20attack%20Kubernetes.pdf)

What would you do if you found a Kubernetes cluster on a penetration test? This presentation focuses on how to get valuable information from a cluster, and do a run-through of a real world scenario where Finn achieved Domain Admin all from a kubernetes config.

### AV Evasion: The Lazy Hacker's Guide with Volkis

_Author: {{ page.max_link }}_ ・ _Event: UTS CSEC_

A deep dive into the laziest way of antivirus evasion using Azure pipelines and open source tools. It covers basic antivirus evasion theory and how to use Azure pipelines. 

## 2022

### Active Directory Hacking Speedrun

_Author: {{ page.alexei_link }}_ ・ _Event: CSECcon 2022_

**Link:** <https://youtu.be/AaOd0XJKEyA>

An all-demo presentation on 14 of the most common attacks against Active Directory. Take this as a starting point to replicate later in your own lab.

### Intro to Capture the Flag (CTF)

_Author: {{ page.max_link }}_ (Francis Dong + DUCTF Team)・ _Event: CSECcon 2022_

**Link:** <https://youtu.be/iU8d37jHKbI>

Everything you need to get started in the wonderful world of participating in CTFs. It also covers a little of Max's CTF journey. 

### Social engineer your way into your first infosec job

_Author: {{ page.alexei_link }}_ ・ _Event: UTS CSEC_

**Link:** <https://youtu.be/Jpcj8eyFpTA>

What do you need to get your first job in infosec? Alexei talks about what you need and don't need, what you should put in your CV/resume, and what to expect in interview. A must-watch for anyone looking to break into infosec.

### Beg Bounty Hall of Fame

_Author: {{ page.alexei_link }}_ ・ _Event: ComfyCon AU 2022_

**Link:** <https://youtu.be/uj3O_rm6HuQ?t=26945>

A look at some of the best (or worst) bad submissions to our vulnerability disclosure program. Why is this such a widespread problem and what can we do about it?

### Pentesting - The first 6 months

_Author: {{ page.nathan_link }}_ ・ _Event: ComfyCon AU 2022_

**Link:** <https://www.youtube.com/watch?v=_p5RuK0Jmpk&t=21945s>

A story about how Nathan got into the world of offensive security, the struggles, and what to expect in the first 6 months.


## 2020

### Zapping bugs in Storage by Zapier

_Author: {{ page.alexei_link }}_ ・ _Event: ComfyCon AU 2020_

**Link:** <https://youtu.be/jey5xY78Hes?t=12077>

Alexei talking about a few vulnerabilities that he discovered in Storage by Zapier. For a bit of fun, he demo's the worst C2 ever written.

### Report Ranger overview

_Author: {{ page.matt_link }}_ ・ _Event: ComfyCon AU 2020_

**Link:** <https://youtu.be/zzovS2FDXe0?t=14246>

An overview of Volkis's Report Ranger tool. Matt goes through how it works, why we designed it and some basic usage example.

### DownUnderCTF: One of Australia's largest CTFs in a nutshell!

_Author: {{ page.max_link }}_ (Sam Calamos, Faith + DUCTF Team) ・ _Event: ComfyCon AU 2020 Summer Edition_

**Link:** <https://youtu.be/zzovS2FDXe0?t=28083>

A deep dive of how 13 Australian Cyber Security university societies came together to keep create one of Australia's largest CTFs.
