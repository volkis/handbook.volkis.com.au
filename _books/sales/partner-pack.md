---
title: Partner pack
owner: Alexei
last_reviewed_on: 2024-01-29
---

Howdy partners! This page is simply a round-up of all the Handbook pages that might be useful to you during (pre)sales.

🎗️ _Remember to bookmark this page_ 🎗️

Here's the list:

- **[Scoping guide](https://handbook.volkis.com.au/sales/scoping-guide/)**: A page explaining how we scope projects and some of the questions we ask. This one is important for you to understand.
- **[Scoping questionnaire](https://handbook.volkis.com.au/assets/doc/Scoping%20Questionnaire%20Template.docx)**: A Word document with scoping questions for some of our commonly taken services. It's blank so feel free to add your branding, but remember, a 30-minutes call is almost always better than simply using the questionnaire.
- **[Anonymised Pentest Report](https://handbook.volkis.com.au/assets/doc/Volkis%20-%20Anonymous%20Client%20-%20Penetration%20Test%20May%202023.pdf)**: A real (but anonymised) pentest report for a real client. Great to use for clients who want an idea of what they'll get.
- **[Sample ACSC Essential 8 Report](https://handbook.volkis.com.au/assets/doc/Volkis%20-%20Sample%20report%20-%20Essential%208%20Maturity%20Model%20Assessment%20May%202022.pdf)**: An example of what a report would look like for an Essential 8 Maturity Model Assessment.
- **[Biographical data sheets](https://handbook.volkis.com.au/people/biographical-data-sheets)**: Mini-CVs for all our consultants. Great to use when a client is concerned about certifications.
- **[Methodologies](https://handbook.volkis.com.au/services/methodologies)**: What we actually do during a project. Detailed, but easy to understand.
- **[Pentest welcome pack](https://handbook.volkis.com.au/services/welcome-packs/penetration-testing-welcome-pack)**: A page for new clients on what to expect from the engagement, such as what they might need to prepare, and communication timelines.
- **[The whole Handbook](https://handbook.volkis.com.au/)**: We're proud of our transparency. Showcasing that to clients could make them feel more comfortable trusting us with their security.

Also, not included here, but available in file form:

- **Penetration Testing Sales Battlecard**: A 2-pager PDF summarising the key points of our pentest services. It can be used a quick reference guide during client meetings.
- **Partnership Service Catalogue**: A very detailed PDF listing all of our service offerings, including pricing.